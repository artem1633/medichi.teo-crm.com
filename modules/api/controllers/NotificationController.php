<?php

namespace app\modules\api\controllers;

use app\models\MobileUser;
use app\models\Notification;
use Yii;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class NotificationController extends Controller
{
    private $user;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     *
     */
    public function actionIndex()
    {
        $models = Notification::find()->all();

        return $models;
    }

    public function beforeAction($action)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $token = null;

        if($request->isPost){
            $token = isset($_POST['token']) ? $_POST['token'] : null;
        } else if ($request->isGet){
            $token = isset($_GET['token']) ? $_GET['token'] : null;
        }

        if($token){
            $this->user = MobileUser::find()->where(['token' => $token])->one();
        }

        if($this->user == null){
            throw new ForbiddenHttpException();
        }

        return parent::beforeAction($action);
    }

}