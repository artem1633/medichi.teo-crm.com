<?php

namespace app\modules\api\controllers;

use app\models\AccountingReport;
use app\models\AffiliateAccounting;
use app\models\Settings;
use app\models\User;
use Yii,
    yii\rest\ActiveController,
    yii\filters\AccessControl,
    yii\web\NotFoundHttpException,
    app\models\YandexMoney,
    app\models\YandexLog,
    app\models\CompanySettings,
    app\models\Companies;


/**
 * Class YandexController
 * @package app\modules\api\controllers
 *
 * @property Client $model
 */
class YandexController extends ActiveController
{
    public $modelClass = 'app\models\Api';
    public $model = null;

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'except' => ['confirm-pay'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        if (is_null($this->model) && !Yii::$app->user->isGuest) {
            $this->model = $this->findClient(Yii::$app->user->identity->entity);
        }
        return parent::actions();
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }



    /**
     * @return string
     */
    public function actionConfirmPay()
    {
        $this->enableCsrfValidation = false;
        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();
            $configure = $this->findModel(1);
            if ($post['label']) echo $post['label'];
            if ($company = User::findOne($post['label'])) {
                $sha1_res = sha1($post['notification_type']
                    . '&' . $post['operation_id']
                    . '&' . $post['amount']
                    . '&' . $post['currency']
                    . '&' . $post['datetime']
                    . '&' . $post['sender']
                    . '&' . $post['codepro']
                    . '&' . $configure->secret
                    . '&' . $post['label']);

                if ($sha1_res == $post['sha1_hash']) {
                    $company->balance_rub += floatval($post['withdraw_amount']);
                    if ($company->save()) {
//                        $report = new AccountingReport([
//                            'company_id' => $company->id,
//                            'operation_type' => AccountingReport::TYPE_INCOME_BALANCE,
//                            'amount' => floatval($post['withdraw_amount']),
//                            'description' => 'Поплнение личного ссчета. Квитанция №' . $post['operation_id']
//                        ]);

//                        $vk_id = Settings::find()->where(['key' => 'akk_notify'])->one()->value;
//                        $proxy = Settings::find()->where(['key' => 'proxy_server'])->one()->value;
//                        $token = Settings::find()->where(['key' => 'vk_access_token'])->one()->value;
//
//                        ClientController::request('messages.send', $proxy, [
//                            'access_token' => $token,
//                            'user_id' => $vk_id,
//                            'message' => "Пополнение лицевого счета компании «{$company->company_name}» на сумму ".floatval($post['withdraw_amount'])." руб
//                        Квитанция №{$post['operation_id']}",
//                        ]);

//                        $report->save();
                    }

//                    $referal = Companies::findOne($company->referal);
//                    $settings = CompanySettings::findOne(['company_id' => $company->referal]);
//                    if ($referal && $settings) {
//                        $persent = ($post['withdraw_amount'] / 100 * floatval($settings->affiliate_percent));
//                        $referal->affiliate_amount += $persent;
//                        if ($referal->save()) {
//                            $reportRef = new AccountingReport([
//                                'company_id' => $referal->id,
//                                'operation_type' => AccountingReport::TYPE_INCOME_AFFILIATE,
//                                'amount' => $persent,
//                                'description' => 'Партнерские отчисления от компании ' . $company->company_name,
//                            ]);
//                            $reportAff = new AffiliateAccounting([
//                                'company_id' => $referal->id,
//                                'referal_id' => $company->id,
//                                'amount' => $persent,
//                            ]);
//                            $reportAff->save();
//                            $reportRef->save();
//                        }
//                    }
                }
            }
        }
    }

    /**
     * @param $id
     * @return null|static
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = YandexMoney::findOne($id)) !== null) {
            return $model;
        } else {
            return new YandexMoney;
        }
    }
}
