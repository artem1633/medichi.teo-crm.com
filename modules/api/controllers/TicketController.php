<?php

namespace app\modules\api\controllers;

use app\models\MobileUser;
use app\models\Ticket;
use app\models\TicketMessage;
use Yii;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\Response;

class TicketController extends Controller
{
    private $user;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    /**
     *
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Ticket();

        if($model->load($request->post()) && $model->validate())
        {
            $model->mobile_user_id = $this->user->id;
        } else {
            return $model->errors;
        }
    }

    /**
     * postId integer
     */
    public function actionGetMessages()
    {
        if(isset($_GET['postId']) == null){
            throw new BadRequestHttpException('postId must be required');
        }

        $model = Ticket::findOne($_GET['postId']);

        if($model == null){
            throw new BadRequestHttpException('invalid postId');
        }

        $messages = TicketMessage::find()->where(['ticket_id' => $model->id])->all();

        return $messages;
    }

    /**
     * postId integer
     * text string
     */
    public function actionSendMessage()
    {
        if(isset($_POST['postId']) == null){
            throw new BadRequestHttpException('postId must be required');
        }

        if(isset($_POST['text']) == null){
            throw new BadRequestHttpException('text must be required');
        }

        /** @var Ticket $model */
        $model = Ticket::findOne($_POST['postId']);

        if($model == null){
            throw new BadRequestHttpException('invalid postId');
        }

        if($model->mobile_user_id != $this->user->id){
            throw new ForbiddenHttpException();
        }

        $message = new TicketMessage([
            'ticket_id' => $model->id,
            'from' => TicketMessage::FROM_USER,
            'text' => $_GET['text']
        ]);
        $result = $message->save(false);

        $model->updateLastMessageDateTime($message->created_at);

        if($result) {
            return $message;
        } else {
            return ['result' => false];
        }
    }

    public function beforeAction($action)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $token = null;

        if($request->isPost){
            $token = isset($_POST['token']) ? $_POST['token'] : null;
        } else if ($request->isGet){
            $token = isset($_GET['token']) ? $_GET['token'] : null;
        }

        if($token){
            $this->user = MobileUser::find()->where(['token' => $token])->one();
        }

        if($this->user == null){
            throw new ForbiddenHttpException();
        }

        return parent::beforeAction($action);
    }
}