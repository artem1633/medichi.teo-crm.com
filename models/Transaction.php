<?php

namespace app\models;

use app\helpers\EmailNotificationHelper;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "transaction".
 *
 * @property int $id
 * @property int $user_id Заявитель
 * @property int $manager_id Операционист
 * @property int $type Тип операции (Пополнение/Вывод)
 * @property int $method Метод
 * @property double $amount Сумма
 * @property int $currency Валюта
 * @property int $purpose_id Назначение
 * @property string $inn ИНН
 * @property string $kpp КПП
 * @property int $is_require_documents Нужны ли документы
 * @property int $is_documents_nds Документы | С НДС или без
 * @property int $document_purpose_id Документы | Назначение
 * @property double $summary_price_amount Итоговая оплата
 * @property int $status Статус
 * @property string $fio ФИО
 * @property string $phone Телефон
 * @property string $town Город
 * @property string $card_number Номер карты
 * @property string $cardholders_name Держатель карты (ФИО на латинице)
 * @property string $file_requisites Файл реквизитов получателя
 * @property string $wallet_number Номер кошелька
 * @property string $manager_accepted_at Дата и врмя принятия к обработке операционистом
 * @property string $created_at
 *
 * @property User $manager
 * @property TransactionPurpose $purpose
 * @property User $user
 */
class Transaction extends \yii\db\ActiveRecord
{
    const EVENT_MANAGER_ACCEPTED = 'manager_accepted';

    const TYPE_IN = 0;
    const TYPE_OUT = 1;

    const METHOD_CORPORATION_CARD = 0;
    const METHOD_CASHLESS = 1;
    const METHOD_UNISTREAM = 2;
    const METHOD_VISA_AND_MASTER_CARD = 3;
    const METHOD_UNIONPAY = 4;
    const METHOD_SWIFT = 5;
    const METHOD_CRYPT = 6;

    const CURRENCY_USD = 0;
    const CURRENCY_MDC = 1;
//    const CURRENCY_USDT = 2;

    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transaction';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['created_at'],
                    self::EVENT_MANAGER_ACCEPTED => ['manager_accepted_at'],
                ],
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'user_id',
                'updatedByAttribute' => null,
                'value' => Yii::$app->user->getId()
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['amount', 'method'], 'required'],
            [['amount'], 'required'],
            [['user_id', 'manager_id', 'type', 'method', 'currency', 'purpose_id', 'is_require_documents', 'is_documents_nds', 'document_purpose_id', 'status'], 'integer'],
            [['amount', 'summary_price_amount'], 'number'],
            [['manager_accepted_at', 'created_at'], 'safe'],
            [['inn', 'kpp', 'fio', 'phone', 'town', 'card_number', 'cardholders_name', 'file_requisites', 'wallet_number'], 'string', 'max' => 255],
            [['file'], 'file', 'skipOnEmpty' => true],
            [['manager_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['manager_id' => 'id']],
            [['purpose_id'], 'exist', 'skipOnError' => true, 'targetClass' => TransactionPurpose::className(), 'targetAttribute' => ['purpose_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Заявитель',
            'manager_id' => 'Операционист',
            'type' => 'Тип операции (Пополнение/Вывод)',
            'method' => 'Метод',
            'amount' => 'Сумма',
            'currency' => 'Валюта',
            'purpose_id' => 'Назначение',
            'inn' => 'ИНН',
            'kpp' => 'КПП',
            'is_require_documents' => 'Нужны ли документы',
            'is_documents_nds' => 'Документы | С НДС или без',
            'document_purpose_id' => 'Документы | Назначение',
            'summary_price_amount' => 'Итоговая оплата',
            'status' => 'Статус',
            'fio' => 'ФИО',
            'phone' => 'Телефон',
            'town' => 'Город',
            'card_number' => 'Номер карты',
            'cardholders_name' => 'Держатель карты (на латинице)',
            'file_requisites' => 'Файл реквизитов получателя',
            'wallet_number' => 'Комментарий',
            'manager_accepted_at' => 'Дата и врмя принятия к обработке операционистом',
            'file' => 'Файл реквизитов получателя',
            'created_at' => 'Дата и время создания',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if($this->file != null)
        {
            if(file_exists('uploads') == false){
                mkdir('uploads');
            }

            $name = Yii::$app->security->generateRandomString();
            $this->file_requisites = "uploads/{$name}.{$this->file->extension}";

            $this->file->saveAs($this->file_requisites);
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($insert){
            if($this->type == self::TYPE_OUT){
                EmailNotificationHelper::notify("Уведомление", "Пользователь под id ".Yii::$app->user->getId()." вывел MDC на сумму в {$this->amount}");
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(User::className(), ['id' => 'manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurpose()
    {
        return $this->hasOne(TransactionPurpose::className(), ['id' => 'purpose_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
