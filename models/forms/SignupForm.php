<?php
namespace app\models\forms;

use app\models\RefAgent;
use yii\base\Model;
use app\models\User;
use Yii;
use yii\helpers\Url;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $userId;
    public $email;
    public $password;
    public $repeatPassword;
    public $ref_id;
    public $phone;
    public $code;



    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password', 'repeatPassword', 'phone'], 'required'],
//            ['code', function(){
//                if(Yii::$app->session->has('confirm_code')){
//                    $confirmCode = Yii::$app->session->get('confirm_code');
//                    if($confirmCode != $this->code){
//                        $this->addError('code', 'Код не верный');
//                    }
//                } else {
//                    $this->addError('code', 'Код не отправлен');
//                }
//            }],
            [['phone'], 'string'],
            ['email', 'trim'],
            ['email', 'email'],
            [['ref_id'], 'integer'],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'targetAttribute' => 'login', 'message' => 'Этот email уже зарегистрирован'],
            ['password', 'string', 'min' => 6],
            ['repeatPassword', 'compare', 'compareAttribute' => 'password', 'message' => 'Пароли не совпадают'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
            'password' => 'Пароль',
            'repeatPassword' => 'Повторите пароль',
            'phone' => 'Номер телефона',
            'code' => 'Код',
            'ref_id' => 'Реферальная ссылка'
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }


//        if(isset($_POST['g-recaptcha-response'])){
//            if($_POST['g-recaptcha-response'] == ''){
//                $this->addError('email', 'Вы не прошли проверку');
//                return false;
//            }
//        }


        $user = new User();
        $user->login = $this->email;
        $user->setPassword($this->password);
        $user->password = $this->password;
        $user->role = User::ROLE_USER;
        $user->ref_id = $this->ref_id;
        $user->phone = $this->phone;
        $user->email_approved = false;
        $user->save(false);

        $text = "Чтобы активировать ваш аккаунт пожалуйста перейдите по ссылке: ".Url::toRoute(['site/confirm', 'id' => $user->id, 'hash' => $user->password_hash], true);

        try {
            Yii::$app->mailer->compose()
                ->setFrom('medichi.notifications@mail.ru')
                ->setTo($this->email)
                ->setSubject("Подтверждение email")
                ->setHtmlBody($text)
                ->send();
        } catch(\Exception $e){
            \Yii::warning($e->getMessage());
            \Yii::warning($text, 'Email text');
        }

        Yii::$app->session->remove('confirm_code');

//        if($this->ref_id){
//            $ref = RefAgent::findOne($this->ref_id);
//            if($ref){
//                $ref->agent_id = $user->id;
//                $ref->save(false);
//            }
//        }

        return $user;
    }

//    /**
//     * update user.
//     *
//     * @param User $user
//     * @return User|null the saved model or null if saving fails
//     */
//    public function update($user)
//    {
//        if (!$this->validate()) {
//            return null;
//        }
//
//        $user->name = $this->name;
//        $user->surname = $this->surname;
//        $user->patronymic = $this->patronymic;
//        $user->phone = $this->phone;
//        $user->category = $this->category;
//        $user->department = $this->department;
//        $user->email = $this->email;
//        $user->setPassword($this->password);
//        $user->password = $this->password;
//        // $user->generateAuthKey();
//
//        return $user->update();
//    }
}
