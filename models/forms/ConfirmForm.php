<?php

namespace app\models\forms;

use app\models\User;
use Yii;
use yii\base\Model;

/**
 * Class ConfirmForm
 * @package app\models\forms
 */
class ConfirmForm extends Model
{
    /** @var string */
    public $code;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code'], 'required'],
            ['code', function(){
                /** @var User $user */
                $user = Yii::$app->user->identity;
                if($user->confirm_code != $this->code){
                    $this->addError('code', 'Код неверный');
                }
            }],
        ];
    }

    /**
     * @return boolean
     */
    public function check()
    {
        if($this->validate()){
            /** @var User $user */
            $user = Yii::$app->user->identity;
            $user->confirm_code = null;
            $user->save(false);

            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'code' => 'СМС Код',
        ];
    }
}