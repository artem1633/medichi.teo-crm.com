<?php

namespace app\models;

use app\helpers\EmailNotificationHelper;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property int $type Тип
 * @property int $status Статус
 * @property string $value Значение
 * @property string $created_at
 * @property string $internal_hash
 *
 * @property User $user
 */
class Order extends \yii\db\ActiveRecord
{
    const STATUS_PRE = -1;
    const STATUS_PLAN = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_CONFIRM = 2;

    const TYPE_BUY = 0;
    const TYPE_SELL = 1;
    const TYPE_INVEST = 2;
    const TYPE_REINVEST = 3;
    const TYPE_DELEGATE_MDC = 4;
    const TYPE_REDELEGATE_MDC = 5;
    const TYPE_INVEST_PROJECT = 6;
    const TYPE_REINVEST_PROJECT_YES = 7;
    const TYPE_REINVEST_PROJECT_NO = 8;
    const TYPE_REPLENISHMENT = 9;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'type', 'status'], 'integer'],
            [['created_at'], 'safe'],
            [['value'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'type' => 'Тип',
            'status' => 'Статус',
            'value' => 'Значение',
            'created_at' => 'Дата и время',
            'internal_hash' => 'Внутренний хеш',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function statusLabels()
    {
        return [
            self::STATUS_PRE => 'Предварительный',
            self::STATUS_PLAN => 'Запланированная',
            self::STATUS_ACTIVE => 'Активированная',
            self::STATUS_CONFIRM => 'На подтверждении'
        ];
    }

    /**
     * @inheritdoc
     */
    public static function typeLabels()
    {
        return [
            self::TYPE_BUY => 'Купить',
            self::TYPE_SELL => 'Продать',
            self::TYPE_INVEST => 'Инвестинг в mdc',
            self::TYPE_REINVEST => 'Реинвестировать в mdc',
            self::TYPE_DELEGATE_MDC => 'Делегировать mdc',
            self::TYPE_REDELEGATE_MDC => 'Ределегировать в mdc',
            self::TYPE_INVEST_PROJECT => 'Инвестировать в проекты',
            self::TYPE_REINVEST_PROJECT_YES => 'Реинвестировать в проекты (Да)',
            self::TYPE_REINVEST_PROJECT_NO => 'Реинвестировать в проекты (Нет)',
            self::TYPE_REPLENISHMENT => 'Пополнение'
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($insert){
            EmailNotificationHelper::notify(
                "Уведомление",
                "Пользователь ".Yii::$app->user->identity->login." (id ".Yii::$app->user->getId().") совершил операцию ".ArrayHelper::getValue(self::typeLabels(), $this->type)." на сумму ".number_format($this->value, 0, '.', ' '));
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
