<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $login Логин
 * @property string $name ФИО
 * @property string $password_hash Зашифрованный пароль
 * @property int $role Роль
 * @property double $balance_rub Баланс (Рубль)
 * @property double $balance_usd Баланс (Доллар США)
 * @property double $balance_eur Баланс (Евро)
 * @property double $balance_cny Баланс (Юань)
 * @property int $rate_id Тариф
 * @property int $ref_id Кто пригласил
 * @property int $is_deletable Можно удалить или нельзя
 * @property string $last_active_datetime Дата и время последней активности
 * @property int $investing_active Активированные
 * @property int $investing_plan Запланированные
 * @property int $investing_confirmed На подтверждении
 * @property double $income_delegate Делегированная
 * @property double $income_grow Рост
 * @property double $income_reinvest Реинвестиционная
 * @property double $every_day_grow Ежедневный прирост
 * @property double $every_hour_grow Ежечастный рост
 * @property double $common_grow Общий рост
 * @property double $balance_income Пополнение
 * @property double $balance_exchange Обменяно
 * @property double $balance_withdraw Выведенно
 * @property string $confirm_code Код подтверждения
 * @property string $confirmed Подтвержден
 * @property string $created_at
 * @property int $is_fake
 * @property int $email_approved
 * @property string $avatar
 *
 * @property Ticket[] $tickets
 * @property User $ref
 * @property User[] $users
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_EDIT = 'edit';

    const ROLE_ADMIN = 1;
    const ROLE_MANAGER = 2;
    const ROLE_USER = 3;

    public $password;

    private $oldPasswordHash;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => ['name', 'login', 'is_deletable', 'password', 'password_hash', 'role', 'balance_rub', 'balance_usd', 'balance_eur', 'balance_cny', 'avatar'],
//            self::SCENARIO_EDIT => ['name', 'is_deletable', 'password_hash', 'role', 'balance_rub', 'balance_usd', 'balance_eur', 'balance_cny', 'avatar'],
            self::SCENARIO_EDIT => ['name', 'phone', 'login', 'is_deletable', 'password', 'password_hash', 'role', 'balance_rub', 'balance_usd', 'balance_eur', 'balance_cny', 'avatar'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login'], 'required'],
            [['password'], 'required', 'on' => self::SCENARIO_DEFAULT],
//            ['password', 'match', 'pattern' => '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,50}$/', 'message' => '{attribute} не соответствует всем параметрам безопасности'],
            [['login'], 'unique'],
            ['login', 'email'],
            [['created_at', 'last_active_datetime'], 'safe'],
            [['is_deletable', 'rate_id', 'investing_active', 'investing_plan', 'investing_confirmed', 'confirmed', 'is_fake', 'email_approved'], 'integer'],
            [['balance_rub', 'balance_usd', 'balance_eur', 'balance_cny', 'income_delegate', 'income_grow', 'income_reinvest', 'every_day_grow', 'every_hour_grow', 'common_grow', 'balance_income', 'balance_exchange', 'balance_withdraw'], 'number'],
            [['login', 'password_hash', 'password', 'name', 'confirm_code', 'avatar'], 'string', 'max' => 255],
            [['ref_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['ref_id' => 'id']],
            [['role'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        parent::beforeDelete();

        $uid = Yii::$app->user->identity->id;

        if($uid == $this->id)
        {
            Yii::$app->session->setFlash('error', "Вы авторизованы под пользователем «{$this->login}». Удаление невозможно!");
            return false;
        }

        if($this->is_deletable == false)
        {
            Yii::$app->session->setFlash('error', "Этот пользователь не может подлежать удалению. Удаление невозможно!");
            return false;
        } else {
            return true;
        }

    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->oldPasswordHash = $this->password_hash;
    }

    /**
     * @return string
     */
    public function getRealAvatar()
    {
        return $this->avatar != null ? $this->avatar : 'img/nouser.png';
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if($this->password != null){
                $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
            } else {
                $this->password_hash = $this->oldPasswordHash;
            }

            return true;
        }
        return false;
    }

    /**
     * @return boolean
     */
    public function isSuperAdmin()
    {
        return $this->role == self::ROLE_ADMIN;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'name' => 'ФИО',
            'password_hash' => 'Password Hash',
            'is_fake' => 'Фейк',
            'password' => 'Пароль',
            'status' => 'Статус',
            'email' => 'Email',
            'balance_rub' => 'Баланс (Рубль)',
            'balance_usd' => 'Баланс (USD)',
            'balance_eur' => 'Баланс (MDC)',
            'balance_cny' => 'Бонусы',
            'rate_id' => 'Тариф',
            'ref_id' => 'Кто пригласил',
            'is_deletable' => 'Удаляемый',
            'phone' => 'Телефон',
            'last_active_datetime' => 'Дата и время последней активности',
            'role' => 'Роль',
            'investing_active' => 'Активированные',
            'investing_plan' => 'Запланированные',
            'investing_confirmed' => 'На подтверждении',
            'income_delegate' => 'Делегированная',
            'income_grow' => 'Рост',
            'income_reinvest' => 'Реинвестиционная',
            'every_day_grow' => 'Ежедневный прирост',
            'every_hour_grow' => 'Ежечастный рост',
            'common_grow' => 'Общий рост',
            'balance_income' => 'Пополнение',
            'balance_exchange' => 'Обменяно',
            'confirm_code' => 'Код подтверждения',
            'confirmed' => 'Подтвержден',
            'balance_withdraw' => 'Выведенно',
            'created_at' => 'Дата и время создания',
            'email_approved' => 'Email подтвержден'
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public function commonIncomeTotal()
    {
        $income = Order::find()->where(['user_id' => $this->id, 'type' => Order::TYPE_REPLENISHMENT])->sum('value');

        return $income * 0.5;
    }

    public function commonIncomeMonth()
    {
        $date = time() - 2.628e+6;
        $income = Order::find()->where(['user_id' => $this->id, 'type' => Order::TYPE_REPLENISHMENT])->andWhere(['<', 'created_at', $date])->sum('value');

        return $income * 0.5;
    }

    public function commonIncomeDay()
    {
        $date = time() - 86400;
        $income = Order::find()->where(['user_id' => $this->id, 'type' => Order::TYPE_REPLENISHMENT])->andWhere(['<', 'created_at', $date])->sum('value');

        return $income * 1.2;
    }

    public function delegateIncomeDay()
    {
        $date = time() - 86400;
        $income = Order::find()->where(['user_id' => $this->id, 'type' => Order::TYPE_DELEGATE_MDC])->andWhere(['<', 'created_at', $date])->sum('value');

        return $income * 1.2;
    }

    public function delegateIncomeMonth()
    {
        $date = time() - 2.628e+6;
        $income = Order::find()->where(['user_id' => $this->id, 'type' => Order::TYPE_DELEGATE_MDC])->andWhere(['<', 'created_at', $date])->sum('value');

        return $income * 1.2;
    }

    public function delegateIncomeAll()
    {
        $income = Order::find()->where(['user_id' => $this->id, 'type' => Order::TYPE_DELEGATE_MDC])->sum('value');

        return $income * 1.2;
    }

    public function delegateIncomeMdcDay()
    {
        $price = floatval(Settings::findByKey('mdc_price')->value);

        return $this->delegateIncomeDay() * $price;
    }

    public function delegateIncomeMdcMonth()
    {
        $price = floatval(Settings::findByKey('mdc_price')->value);

        return $this->delegateIncomeMonth() * $price;
    }

    public function delegateIncomeMdcAll()
    {
        $price = floatval(Settings::findByKey('mdc_price')->value);

        return $this->delegateIncomeAll() * $price;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTickets()
    {
        return $this->hasMany(Ticket::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRef()
    {
        return $this->hasOne(User::className(), ['id' => 'ref_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['ref_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['login' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getPermmission()
    {
        return $this->stat_indet;
    }


    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->password;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return boolean
     */
    public function isOnline()
    {
        if($this->last_active_datetime == null) {
            return false;
        }

        $nowDt = new \DateTime("now", new \DateTimeZone(\Yii::$app->timeZone));
        $seen = new \DateTime($this->last_active_datetime, new \DateTimeZone(\Yii::$app->timeZone));

        return $nowDt->getTimestamp() - $seen->getTimestamp() < 2 * 60;
    }
}
