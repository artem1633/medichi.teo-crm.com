<?php

namespace app\models;

use app\helpers\EmailNotificationHelper;
use Yii;

/**
 * This is the model class for table "ticket_message".
 *
 * @property int $id
 * @property int $ticket_id Тикет
 * @property string $text Текст
 * @property int $from Кто отправил
 * @property string $created_at Дата и время
 *
 * @property Ticket $ticket
 */
class TicketMessage extends \yii\db\ActiveRecord
{
    const FROM_USER = 0;
    const FROM_MANAGER = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket_message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ticket_id', 'from'], 'integer'],
            [['text'], 'string'],
            [['created_at'], 'safe'],
            [['ticket_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ticket::className(), 'targetAttribute' => ['ticket_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ticket_id' => 'Тикет',
            'text' => 'Текст',
            'from' => 'Кто отправил',
            'created_at' => 'Дата и время',
            'attachment' => 'Файл',
        ];
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($this->from == self::FROM_USER){
            EmailNotificationHelper::notify("Уведомление", "Пользователь под id ". Yii::$app->user->getId() ." отправил новое сообщение в тикете: «{$this->text}»");
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicket()
    {
        return $this->hasOne(Ticket::className(), ['id' => 'ticket_id']);
    }
}
