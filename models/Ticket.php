<?php

namespace app\models;

use app\helpers\EmailNotificationHelper;
use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "ticket".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property string $subject Заголовок
 * @property string $description Описание
 * @property string $status Статус
 * @property int $is_read Прочитано
 * @property string $last_message_datetime Дата и время последнего сообщения
 * @property string $created_at Дата и время создания
 * @property string $closed_datetime Дата и время закрытия
 * @property int $notified Уведомлен
 *
 * @property User $user
 * @property TicketMessage[] $ticketMessages
 */
class Ticket extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_WORK = 1;
    const STATUS_DONE = 2;
    const STATUS_REJECTED = 3;

    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'is_read', 'notified'], 'integer'],
            [['description'], 'string'],
            [['last_message_datetime', 'created_at', 'closed_datetime'], 'safe'],
            [['subject', 'status'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['file'], 'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'subject' => 'Заголовок',
            'description' => 'Описание',
            'status' => 'Статус',
            'is_read' => 'Прочитано',
            'last_message_datetime' => 'Дата и время последнего сообщения',
            'created_at' => 'Дата и время создания',
            'closed_datetime' => 'Дата и время закрытия',
            'notified' => 'Уведомлен',
            'file' => 'Файл',
        ];
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_NEW => 'Новый',
            self::STATUS_WORK => 'В работе',
            self::STATUS_DONE => 'Выполнено',
            self::STATUS_REJECTED => 'Отклонен',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if($this->isNewRecord && Yii::$app->user->identity->isSuperAdmin() == false){
            $this->user_id = Yii::$app->user->getId();
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if($insert){
            (new Log([
                'user_id' => Yii::$app->user->getId(),
                'content' => "Создал тикет «{$this->subject}»"
            ]))->save();
            EmailNotificationHelper::notify("Уведомление", "Пользователь под id ". Yii::$app->user->getId() ." создал тикет «{$this->subject}»");

//            $file = UploadedFile::getInstance($this, 'file');
//            $fileName = Yii
//
//            (new TicketMessage([
//                'ticket_id' => $this->id,
//                'text' => $this->description,
//                'from' => TicketMessage::FROM_USER,
//                'created_at' => date('Y-m-d H:i:s'),
//                'file' => ,
//            ]))->save(false);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicketMessages()
    {
        return $this->hasMany(TicketMessage::className(), ['ticket_id' => 'id']);
    }
}
