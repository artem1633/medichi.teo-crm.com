<?php

namespace app\helpers;

use app\models\Settings;
use Yii;

/**
 * Class EmailNotificationHelper
 * @package app\helpers
 */
class EmailNotificationHelper
{
    /**
     * @param string $subject
     * @param string $text
     */
    public static function notify($subject, $text)
    {
        $adminLogin = Settings::findByKey('admin_email')->value;

        if($adminLogin){
            try {
                Yii::$app->mailer->compose()
                    ->setFrom('medichi.notifications@mail.ru')
                    ->setTo($adminLogin)
                    ->setSubject($subject)
                    ->setHtmlBody($text)
                    ->send();
            } catch(\Exception $e){
                \Yii::warning($e->getMessage());
            }
        }
    }
}