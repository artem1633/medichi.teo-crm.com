<?php

namespace app\widgets;

use app\models\User;
use yii\base\InvalidConfigException;
use yii\base\Widget;

/**
 * Class UserBalances
 * @package app\widgets
 */
class UserBalances extends Widget
{
    /**
     * @var User
     */
    public $user;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if($this->user == null){
            throw new InvalidConfigException('user must be required');
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        parent::run();

        $html = '';
        $html .= "<p>{$this->user->balance_rub} <i class='fa fa-rub'></i></p>";
        $html .= "<p>{$this->user->balance_usd} <i class='fa fa-btc'></i></p>";
        $html .= "<p>{$this->user->balance_eur} MDC";

        return $html;
    }
}