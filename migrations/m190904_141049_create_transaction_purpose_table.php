<?php

use yii\db\Migration;

/**
 * Handles the creation of table `transaction_purpose`.
 */
class m190904_141049_create_transaction_purpose_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('transaction_purpose', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->comment('Наименование'),
            'user_id' => $this->integer()->comment('Пользователь'),
        ]);

        $this->createIndex(
            'idx-transaction_purpose-user_id',
            'transaction_purpose',
            'user_id'
        );

        $this->addForeignKey(
            'fk-transaction_purpose-user_id',
            'transaction_purpose',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-transaction_purpose-user_id',
            'transaction_purpose'
        );

        $this->dropIndex(
            'idx-transaction_purpose-user_id',
            'transaction_purpose'
        );

        $this->dropTable('transaction_purpose');
    }
}
