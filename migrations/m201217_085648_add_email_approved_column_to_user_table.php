<?php

use yii\db\Migration;

/**
 * Handles adding email_approved to table `user`.
 */
class m201217_085648_add_email_approved_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'email_approved', $this->boolean()->defaultValue(false)->comment('Email подтвержден'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'email_approved');
    }
}
