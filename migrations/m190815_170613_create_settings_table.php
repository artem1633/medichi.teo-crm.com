<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m190815_170613_create_settings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->notNull()->unique()->comment('Ключ'),
            'type' => $this->string()->defaultValue(\app\models\Settings::TYPE_TEXT)->comment('Тип'),
            'value' => $this->string()->comment('Значение'),
            'label' => $this->text()->comment('Комментарий'),
        ]);

        $this->insert('settings', [
            'key' => 'level_1',
            'value' => '4',
            'label' => 'Уровень 1',
        ]);
        $this->insert('settings', [
            'key' => 'level_2',
            'value' => '2',
            'label' => 'Уровень 2',
        ]);
        $this->insert('settings', [
            'key' => 'level_3',
            'value' => '0.5',
            'label' => 'Уровень 3',
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('settings');
    }
}
