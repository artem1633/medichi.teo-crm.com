<?php

use yii\db\Migration;

/**
 * Handles adding avatar to table `user`.
 */
class m201217_095523_add_avatar_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'avatar', $this->string()->comment('Аватар'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'avatar');
    }
}
