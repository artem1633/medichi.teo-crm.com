<?php

use yii\db\Migration;

/**
 * Handles adding attachment to table `ticket_message`.
 */
class m201224_100103_add_attachment_column_to_ticket_message_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('ticket_message', 'attachment', $this->string()->comment('Файл'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('ticket_message', 'attachment');
    }
}
