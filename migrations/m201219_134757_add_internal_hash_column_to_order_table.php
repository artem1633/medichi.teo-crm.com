<?php

use yii\db\Migration;

/**
 * Handles adding internal_hash to table `order`.
 */
class m201219_134757_add_internal_hash_column_to_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('order', 'internal_hash', $this->string()->comment('Внутренний хеш'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('order', 'internal_hash');
    }
}
