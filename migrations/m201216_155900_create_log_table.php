<?php

use yii\db\Migration;

/**
 * Handles the creation of table `log`.
 */
class m201216_155900_create_log_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('log', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->comment('Пользователь'),
            'content' => $this->text()->comment('Текст'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-log-user_id',
            'log',
            'user_id'
        );

        $this->addForeignKey(
            'fk-log-user_id',
            'log',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-log-user_id',
            'log'
        );

        $this->dropIndex(
            'idx-log-user_id',
            'log'
        );

        $this->dropTable('log');
    }
}
