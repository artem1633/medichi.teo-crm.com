<?php

use yii\db\Migration;

/**
 * Class m201217_084200_add_perfect_money_settings
 */
class m201217_084200_add_perfect_money_settings extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'pm_payee_account',
            'value' => '',
            'label' => 'Perfect Money | Аккаунт',
        ]);

        $this->insert('settings', [
            'key' => 'pm_payment_units',
            'value' => '',
            'label' => 'Perfect Money | Валюта',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        \app\models\Settings::deleteAll(['key' => ['pm_payee_account', 'pm_payment_units']]);
    }
}
