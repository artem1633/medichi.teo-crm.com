<?php

use yii\db\Migration;

/**
 * Class m201216_125813_add_mdc_price_setting
 */
class m201216_125813_add_mdc_price_setting extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'mdc_price',
            'value' => '',
            'label' => 'Курс MDC',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        \app\models\Settings::deleteAll(['key' => 'mdc_price']);
    }
}
