<?php

use yii\db\Migration;

/**
 * Handles adding is_fake to table `user`.
 */
class m201216_153039_add_is_fake_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'is_fake', $this->boolean()->defaultValue(false)->comment('Фейк'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'is_fake');
    }
}
