<?php

use yii\db\Migration;

/**
 * Handles adding notified to table `ticket`.
 */
class m201217_151114_add_notified_column_to_ticket_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('ticket', 'notified', $this->boolean()->defaultValue(false)->comment('Уведомлен'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('ticket', 'notified');
    }
}
