<?php

use yii\db\Migration;

/**
 * Class m201218_140852_add_new_settings
 */
class m201218_135652_add_new_settings extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'company_money_all',
            'value' => '',
            'label' => 'Компания заработала (Всего)',
        ]);
        $this->insert('settings', [
            'key' => 'company_money_month',
            'value' => '',
            'label' => 'Компания заработала (За месяц)',
        ]);
        $this->insert('settings', [
            'key' => 'company_money_day',
            'value' => '',
            'label' => 'Компания заработала (За сутки)',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        \app\models\Settings::deleteAll(['key' => ['company_money_all', 'company_money_month', 'company_money_day']]);
    }
}
