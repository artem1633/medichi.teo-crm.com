<?php

use yii\db\Migration;

/**
 * Class m201216_160015_add_admin_email_setting
 */
class m201216_154515_add_admin_email_setting extends Migration
{
    /**
    * @inheritdoc
    */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'admin_email',
            'value' => '',
            'label' => 'Почта администратора (для уведомлений)',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        \app\models\Settings::deleteAll(['key' => 'admin_email']);
    }
}
