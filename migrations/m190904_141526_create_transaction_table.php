<?php

use yii\db\Migration;

/**
 * Handles the creation of table `transaction`.
 */
class m190904_141526_create_transaction_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('transaction', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull()->comment('Заявитель'),
            'manager_id' => $this->integer()->comment('Операционист'),
            'type' => $this->integer()->comment('Тип операции (Пополнение/Вывод)'),
            'method' => $this->integer()->comment('Метод'),
            'amount' => $this->float()->comment('Сумма'),
            'currency' => $this->integer()->comment('Валюта'),
            'purpose_id' => $this->integer()->comment('Назначение'),
            'inn' => $this->string()->comment('ИНН'),
            'kpp' => $this->string()->comment('КПП'),
            'is_require_documents' => $this->boolean()->comment('Нужны ли документы'),
            'is_documents_nds' => $this->boolean()->comment('Документы | С НДС или без'),
            'document_purpose_id' => $this->integer()->comment('Документы | Назначение'),
            'summary_price_amount' => $this->float()->comment('Итоговая оплата'),
            'status' => $this->integer()->comment('Статус'),
            'fio' => $this->string()->comment('ФИО'),
            'phone' => $this->string()->comment('Телефон'),
            'town' => $this->string()->comment('Город'),
            'card_number' => $this->string()->comment('Номер карты'),
            'cardholders_name' => $this->string()->comment('Держатель карты (ФИО на латинице)'),
            'file_requisites' => $this->string()->comment('Файл реквизитов получателя'),
            'wallet_number' => $this->string()->comment('Номер кошелька'),
            'manager_accepted_at' => $this->dateTime()->comment('Дата и врмя принятия к обработке операционистом'),
            'created_at' => $this->dateTime(),
        ]);

        $this->createIndex(
            'idx-transaction-user_id',
            'transaction',
            'user_id'
        );

        $this->addForeignKey(
            'fk-transaction-user_id',
            'transaction',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-transaction-manager_id',
            'transaction',
            'manager_id'
        );

        $this->addForeignKey(
            'fk-transaction-manager_id',
            'transaction',
            'manager_id',
            'user',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-transaction-purpose_id',
            'transaction',
            'purpose_id'
        );

        $this->addForeignKey(
            'fk-transaction-purpose_id',
            'transaction',
            'purpose_id',
            'transaction_purpose',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-transaction-purpose_id',
            'transaction'
        );

        $this->dropIndex(
            'idx-transaction-purpose_id',
            'transaction'
        );

        $this->dropForeignKey(
            'fk-transaction-manager_id',
            'transaction'
        );

        $this->dropIndex(
            'idx-transaction-manager_id',
            'transaction'
        );

        $this->dropForeignKey(
            'fk-transaction-user_id',
            'transaction'
        );

        $this->dropIndex(
            'idx-transaction-user_id',
            'transaction'
        );

        $this->dropTable('transaction');
    }
}
