<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170428_140722_create_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'login' => $this->string()->notNull()->comment('Логин'),
            'name' => $this->string()->comment('ФИО'),
            'password_hash' => $this->string()->notNull()->comment('Зашифрованный пароль'),
            'role' => $this->integer()->comment('Роль'),
            'balance_rub' => $this->float()->unsigned()->defaultValue(0)->comment('Баланс (Рубль)'),
            'balance_usd' => $this->float()->unsigned()->defaultValue(0)->comment('Баланс (Доллар США)'),
            'balance_eur' => $this->float()->unsigned()->defaultValue(0)->comment('Баланс (Евро)'),
            'balance_cny' => $this->float()->unsigned()->defaultValue(0)->comment('Баланс (Юань)'),
            'confirm_code' => $this->string()->comment('Код подтверждения'),
            'rate_id' => $this->integer()->comment('Тариф'),
            'ref_id' => $this->integer()->comment('Кто пригласил'),
            'is_deletable' => $this->boolean()->notNull()->defaultValue(true)->comment('Можно удалить или нельзя'),
            'last_active_datetime' => $this->dateTime()->comment('Дата и время последней активности'),
            'phone' => $this->string()->comment('Телефон'),
            'created_at' => $this->dateTime(),
        ]);

        $this->insert('user', [
            'login' => 'admin',
            'password_hash' => Yii::$app->security->generatePasswordHash('admin'),
            'role' => 1,
            'is_deletable' => false,
        ]);

        $this->createIndex(
            'idx-user-ref_id',
            'user',
            'ref_id'
        );

        $this->addForeignKey(
            'fk-user-ref_id',
            'user',
            'ref_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-user-ref_id',
            'user'
        );

        $this->dropIndex(
            'idx-user-ref_id',
            'user'
        );

        $this->dropTable('user');
    }
}
