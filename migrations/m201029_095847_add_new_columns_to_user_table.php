<?php

use yii\db\Migration;

/**
 * Handles adding new to table `user`.
 */
class m201029_095847_add_new_columns_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'investing_active', $this->integer()->comment('Активированные'));
        $this->addColumn('user', 'investing_plan', $this->integer()->comment('Запланированные'));
        $this->addColumn('user', 'investing_confirmed', $this->integer()->comment('На подтверждении'));

        $this->addColumn('user', 'income_delegate', $this->float()->comment('Делегированная'));
        $this->addColumn('user', 'income_grow', $this->float()->comment('Рост'));
        $this->addColumn('user', 'income_reinvest', $this->float()->comment('Реинвестиционная'));

        $this->addColumn('user', 'every_day_grow', $this->float()->comment('Ежедневный прирост'));
        $this->addColumn('user', 'every_hour_grow', $this->float()->comment('Ежечастный рост'));
        $this->addColumn('user', 'common_grow', $this->float()->comment('Общий рост'));

        $this->addColumn('user', 'balance_income', $this->float()->comment('Пополнение'));
        $this->addColumn('user', 'balance_exchange', $this->float()->comment('Обменяно'));
        $this->addColumn('user', 'balance_withdraw', $this->float()->comment('Выведенно'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'investing_active');
        $this->dropColumn('user', 'investing_plan');
        $this->dropColumn('user', 'investing_confirmed');
        $this->dropColumn('user', 'income_delegate');
        $this->dropColumn('user', 'income_grow');
        $this->dropColumn('user', 'income_reinvest');
        $this->dropColumn('user', 'every_day_grow');
        $this->dropColumn('user', 'every_hour_grow');
        $this->dropColumn('user', 'common_grow');
        $this->dropColumn('user', 'balance_income');
        $this->dropColumn('user', 'balance_exchange');
        $this->dropColumn('user', 'balance_withdraw');
    }
}
