<?php

/**
 * @var $this \yii\web\View
 * @var $settings \app\models\Settings[]
 */

$this->title = 'Настройки';

?>


<div class="card">
    <div class="card-body">
        <div class="card-title">Настройки
        </div>
                    <?php if (count($settings) > 0) { ?>
                        <form method="post" class="form-horizontal" style="padding: 0;">
                            <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
                                   value="<?= Yii::$app->request->csrfToken ?>">
                            <?php foreach ($settings as $setting): ?>
                                <div class="form-group">
                                    <label for=""
                                           class="control-label col-md-2"><?= Yii::$app->formatter->asRaw($setting->label) ?></label>
                                    <div class="col-md-10">
                                        <div class="col-md-8">
                                            <?php if($setting->type == null || $setting->type == \app\models\Settings::TYPE_TEXT): ?>
                                                <input name="Settings[<?= $setting->key ?>]" type="text"
                                                       value="<?= $setting->value ?>" class="form-control">
                                            <?php endif; ?>
                                            <?php if($setting->type == \app\models\Settings::TYPE_CHECKBOX): ?>
                                                <input name="Settings[<?= $setting->key ?>]" type="checkbox"
                                                    <?= ($setting->value == 1 ? 'checked=""' : '') ?> class="switch">
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                            <div class="form-group">
                                <label for="" class="control-label col-md-2"></label>
                                <div class="col-md-10">
                                    <div class="input-group">
                                        <button type="submit" style="margin-left: 15px;"
                                                class="btn btn-success btn-sm">Сохранить
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    <?php } else { ?>
                        <div class="note note-danger" style="margin: 15px;">
                            <h4><i class="fa fa-exclamation-triangle"></i> Настроек в базе данных не обнаружено!
                            </h4>
                            <p>
                                Убедитесь, что все миграции выполнены без ошибок
                            </p>
                        </div>
                    <?php } ?>
        </div>
    </div>
</div>
