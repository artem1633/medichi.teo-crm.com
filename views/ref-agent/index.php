<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;
use app\models\Order;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RefAgentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Агенты";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

$refLink = Url::toRoute(['/site/register', 'ref_id' => Yii::$app->user->getId()], true);


/**
 * @param double $userId
 * @param double $percent
 * @return double
 */
function referalIncome($userId, $percent)
{
    $value = Order::find()->where(['type' => [Order::TYPE_INVEST, Order::TYPE_INVEST_PROJECT], 'user_id' => $userId])->sum('value');

    return $value / 100 * $percent;
}

?>

<div class="row pt-2 pb-2">
    <div class="col-sm-9">
        <h5 class="page-title">Мои рефералы</h5>
        <p>Рефералы – это пользователи, зарегистрированные по Вашей реферальной ссылке.
Приглашайте друзей и знакомых - и получайте бонус в размере 4% от внесённой и заделегированной ими суммы! Вы также получите дополнительные бонусы за приглашённых Вашими рефералами пользователей.
</p>
        <hr>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <p>
                    Кто пригласил: <?= \yii\helpers\ArrayHelper::getValue(Yii::$app->user->identity, 'ref.login') ?>
                </p>
                <p>
                    Ваша реферальная ссылка:
                </p>
                <div class="input-group" style="width: 40%;">
                    <input type="text" class="form-control" name="" value="<?=$refLink?>" readonly>
                    <div class="input-group-append">
                        <a class="btn btn-light" href="/dashboard/#" data-new="0" style="padding-top: 9px;" onclick="event.preventDefault(); copyTextToClipboard('<?=$refLink?>'); alert('Скопировано в буфер обмена');"><i class="zmdi zmdi-copy"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h6 class="text-uppercase">УРОВЕНЬ 1 = БОНУС 4%</h6>
        <hr>
        <div id="wrapper-1" class="wrapper" style="width: 100%; white-space: nowrap; overflow-x: auto;">

            <?php

            $refs = isset($childPks[1]) ? $childPks[1] : [];

            $userRefs = \app\models\User::find()->where(['id' => $refs])->all();

            ?>

            <?php if(count($userRefs) > 0): ?>
                <?php foreach ($userRefs as $userRef): ?>
                    <div class="card" style="display: inline-block; width: 450px;">
                        <div class="card-body">
                            <h5><i class="zmdi zmdi-account"></i> <?=$userRef->name?></h5>
                            <p><?=$userRef->login?> </p>
                            <p style="font-size: 13px; margin-bottom: 2px;">Дата и время регистрации: <?=Yii::$app->formatter->asDate($userRef->created_at, 'php:H:i d.m.Y')?> </p>
                            <p style="font-size: 13px; margin-bottom: 2px;">Общая сумма пополнения: <?=Yii::$app->formatter->asDecimal($userRef->common_grow)?> </p>
                            <p style="font-size: 13px; margin-bottom: 2px;">Делегировано: <?=Yii::$app->formatter->asDecimal($userRef->income_delegate)?> </p>
                            <p style="font-size: 13px; margin-bottom: 2px;">Заработок с реферала: <?=Yii::$app->formatter->asDecimal(referalIncome($userRef->id, 4))?> </p>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <h6 style="opacity: .7; margin-left: 30px;">Рефералов пока нет</h6>
            <?php endif; ?>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h6 class="text-uppercase" style="margin-top: 25px;">УРОВЕНЬ 2 = БОНУС 2%</h6>
        <hr>
        <div id="wrapper-2" class="wrapper" style="width: 100%; white-space: nowrap; overflow-x: auto;">

            <?php

            $refs = isset($childPks[2]) ? $childPks[2] : [];

            $userRefs = \app\models\User::find()->where(['id' => $refs])->all();

            ?>

            <?php if(count($userRefs) > 0): ?>
                <?php foreach ($userRefs as $userRef): ?>
                    <div class="card" style="display: inline-block; width: 450px;">
                        <div class="card-body">
                            <h5><i class="zmdi zmdi-account"></i> <?=$userRef->name?></h5>
                            <p><?=$userRef->login?> </p>
                            <p style="font-size: 13px; margin-bottom: 2px;">Дата и время регистрации: <?=Yii::$app->formatter->asDate($userRef->created_at, 'php:H:i d.m.Y')?> </p>
                            <p style="font-size: 13px; margin-bottom: 2px;">Общая сумма пополнения: <?=Yii::$app->formatter->asDecimal($userRef->common_grow)?> </p>
                            <p style="font-size: 13px; margin-bottom: 2px;">Делегировано: <?=Yii::$app->formatter->asDecimal($userRef->income_delegate)?> </p>
                            <p style="font-size: 13px; margin-bottom: 2px;">Заработок с реферала: <?=Yii::$app->formatter->asDecimal(referalIncome($userRef->id, 2))?> </p>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <h6 style="opacity: .7; margin-left: 30px;">Рефералов пока нет</h6>
            <?php endif; ?>

        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h6 class="text-uppercase" style="margin-top: 25px;">УРОВЕНЬ 3 = БОНУС 0.5%</h6>
        <hr>
        <div id="wrapper-3" class="wrapper" style="width: 100%; white-space: nowrap; overflow-x: auto;">

            <?php

            $refs = isset($childPks[3]) ? $childPks[3] : [];

            $userRefs = \app\models\User::find()->where(['id' => $refs])->all();

            ?>

            <?php if(count($userRefs) > 0): ?>
                <?php foreach ($userRefs as $userRef): ?>
                    <div class="card" style="display: inline-block; width: 450px;">
                        <div class="card-body">
                            <h5><i class="zmdi zmdi-account"></i> <?=$userRef->name?></h5>
                            <p><?=$userRef->login?> </p>
                            <p style="font-size: 13px; margin-bottom: 2px;">Дата и время регистрации: <?=Yii::$app->formatter->asDate($userRef->created_at, 'php:H:i d.m.Y')?> </p>
                            <p style="font-size: 13px; margin-bottom: 2px;">Общая сумма пополнения: <?=Yii::$app->formatter->asDecimal($userRef->common_grow)?> </p>
                            <p style="font-size: 13px; margin-bottom: 2px;">Делегировано: <?=Yii::$app->formatter->asDecimal($userRef->income_delegate)?> </p>
                            <p style="font-size: 13px; margin-bottom: 2px;">Заработок с реферала: <?=Yii::$app->formatter->asDecimal(referalIncome($userRef->id, 0.5))?> </p>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <h6 style="opacity: .7; margin-left: 30px;">Рефералов пока нет</h6>
            <?php endif; ?>

        </div>
    </div>
</div>



<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<?php

$script = <<< JS
new SimpleBar(document.getElementById('wrapper-1'));
new SimpleBar(document.getElementById('wrapper-2'));
new SimpleBar(document.getElementById('wrapper-3'));
JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>