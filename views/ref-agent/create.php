<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefAgent */

?>
<div class="ref-agent-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
