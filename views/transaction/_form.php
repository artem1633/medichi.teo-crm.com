<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Transaction */
/* @var $form yii\widgets\ActiveForm */
/* @var $settings \app\components\Settings */

$settings = Yii::$app->settings;

$model->type = \app\models\Transaction::TYPE_IN;

?>

<div class="transaction-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Создание</h4>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'amount')->textInput() ?>
                </div>
            </div>

            <div class="hidden">
                <?= $form->field($model, 'type')->hiddenInput() ?>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'method')->widget(\app\widgets\ButtonsSelect::className(), [
                        'data' => [
                            \app\models\Transaction::METHOD_CORPORATION_CARD => 'Коропоративная карта',
                            \app\models\Transaction::METHOD_CASHLESS => 'Безналичный расчет'
                        ],
                    ]) ?>

                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label">Сумма к оплате</label>
                        <input id="payment-sum" class="form-control" type="text" disabled>
                    </div>
                </div>
            </div>

            <div id="method-cashless-wrapper" <?=$model->method != 1 ? 'style="display: none;"' : ''?>>
                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'purpose_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\TransactionPurpose::find()->where(['user_id' => Yii::$app->user->getId()])->all(), 'id', 'name')) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'inn')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'kpp')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?= $form->field($model, 'is_require_documents')->widget(\app\widgets\ButtonsSelect::className(), [
                        'data' => [
                            0 => 'Нет',
                            1 => 'Да',
                        ],
                    ]) ?>
                </div>
            </div>

            <div id="require-document-wrapper" <?=$model->is_require_documents != 1 ? 'style="display: none;"' : ''?>>
                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'purpose_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\TransactionPurpose::find()->where(['user_id' => Yii::$app->user->getId()])->all(), 'id', 'name')) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'inn')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'kpp')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
            </div>

        </div>
        <div class="panel-footer">
            <?php if (!Yii::$app->request->isAjax){ ?>
                <div class="form-group">
                    <?= Html::submitButton($model->isNewRecord ? 'Отправить заявку' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            <?php } ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    
</div>

<?php

$script = <<< JS
    var comissionCorporationCard = {$settings->commission_corporation_card};
    var comissionCashless = {$settings->commission_cashless};
    var comissionDocuments = 0;
    
    var currentPercent = comissionCorporationCard;
    
    $('[name="Transaction[method]"]').change(function(){
        var value = $(this).val();
        
        if(value == 0){
            currentPercent = comissionCorporationCard;
        } else if(value == 1) {
            currentPercent = comissionCashless;
        }
        
        if(value == 1){
            $('#method-cashless-wrapper').slideDown();
        } else {
            $('#method-cashless-wrapper').slideUp();
        }
        
        var amount = $('#transaction-amount').val();
        
        var sum = amount - (Math.round(amount / 100 * currentPercent) + Math.round(amount / 100 * comissionDocuments));
        
        $('#payment-sum').val(sum);
    });

    $('[name="Transaction[is_require_documents]"]').change(function(){
        var value = $(this).val();
        
        var amount = $('#transaction-amount').val();
        
        if(value == 1){
            comissionDocuments = {$settings->commission_documents};
            $('#require-document-wrapper').slideDown();
        } else {
            comissionDocuments = 0;
            $('#require-document-wrapper').slideUp();
        }
        
         var sum = amount - (Math.round(amount / 100 * currentPercent) + Math.round(amount / 100 * comissionDocuments));
        
        $('#payment-sum').val(sum);
    });
    
    $('#transaction-amount').keyup(function() {
        var value = $(this).val();
        
        var sum = value - (Math.round(value / 100 * currentPercent) + Math.round(value / 100 * comissionDocuments));
        
        $('#payment-sum').val(sum);
    });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>