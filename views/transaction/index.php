<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Заявки";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

$createButton = '';
if(Yii::$app->controller->id == 'transaction-in'){
    $this->title = 'Заявки на пополнение';
    $createButton = Html::a('Пополнение', ['create', 'type' => 0],
        ['title'=> 'Добавить заявку','class'=>'btn btn-success']);
} else if(Yii::$app->controller->id == 'transaction-out') {
    $this->title = 'Заявки на вывод';
    $createButton = Html::a('Вывод', ['create', 'type' => 1],
        ['title'=> 'Добавить заявку','class'=>'btn btn-success']);
}

?>
<div class="panel panel-inverse transaction-index">
    <div class="panel-heading">
<!--        <div class="panel-heading-btn">-->
<!--        </div>-->
        <h4 class="panel-title">Заявки</h4>
    </div>
    <div class="panel-body">
        <div id="ajaxCrudDatatable">
            <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns.php'),
//            'panelBeforeTemplate' => $createButton.'&nbsp;'.
//                Html::a('<i class="fa fa-repeat"></i>', [''],
//                    ['data-pjax'=>1, 'class'=>'btn btn-white', 'title'=>'Обновить']),
                'panelBeforeTemplate' => '',
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
            'after'=>BulkButtonWidget::widget([
            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
            ["bulk-delete"] ,
            [
            "class"=>"btn btn-danger btn-xs",
            'role'=>'modal-remote-bulk',
            'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
            'data-request-method'=>'post',
            'data-confirm-title'=>'Вы уверены?',
            'data-confirm-message'=>'Вы действительно хотите удалить данный элемент?'
            ]),
            ]).
            '<div class="clearfix"></div>',
            ]
            ])?>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
