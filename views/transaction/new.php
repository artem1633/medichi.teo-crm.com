<?php

/** @var $model \app\models\Transaction */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>


<?php $form = ActiveForm::begin(); ?>

<?php if($model->currency == \app\models\Transaction::CURRENCY_USD): ?>

    <p>Вы можете отправить заявку на вывод Ваших незаделегированных денежных средств с баланса Вашего счёта.
        Для этого укажите сумму и номер карты для перевода.
        В Настоящий момент операции на вывод осуществляются Администратором.
        По всем вопросам обращайтесь в службу поддержки.
    </p>

<?php elseif($model->currency == \app\models\Transaction::CURRENCY_MDC): ?>

    <p>Приобретайте нашу собственную DpoS-монету и получайте дополнительный доход на росте курса MDC. Прогнозируемый рост = 5% в месяц.
    </p>

<?php endif; ?>


<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'amount')->textInput() ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'card_number')->textInput() ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= $form->field($model, 'wallet_number')->textInput() ?>
    </div>
</div>
<div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? 'Отправить заявку на вывод' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>

<?php ActiveForm::end(); ?>
