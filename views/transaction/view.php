<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Transaction */
?>
<div class="transaction-view">
 
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Информация</h4>
        </div>
        <div class="panel-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'user.login',
                    'manager.login',
                    'type',
                    'method',
                    'amount',
                    'currency',
                    [
                        'attribute' => 'purpose.name',
                        'label' => 'Назначение'
                    ],
                    'inn',
                    'kpp',
                    'is_require_documents',
                    'is_documents_nds',
                    'document_purpose_id',
                    'summary_price_amount',
                    'status',
                    'fio',
                    'phone',
                    'town',
                    'card_number',
                    'cardholders_name',
                    'file_requisites',
                    'wallet_number',
                    'manager_accepted_at',
                    [
                        'attribute' => 'created_at',
                        'format' => ['date', 'php:d M Y H:i:s'],
                    ],
                ],
            ]) ?>
        </div>
    </div>

</div>
