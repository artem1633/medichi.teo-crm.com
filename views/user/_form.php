<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */


?>


<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->textInput(['maxlength' => true, 'data-toggle' => 'tooltip'])->hint('
        Пароль должен содержать:
        <ul>
            <li>только латинские буквы</li>
            <li>минимум 6 символов</li>
            <li>миним одну заглавную букву</li>
            <li>миним одну цифру</li>
        </ul>
    ') ?>

    <?php
//    echo $form->field($model, 'role')->dropDownList([
//            \app\models\User::ROLE_ADMIN => 'Админ',
//            \app\models\User::ROLE_MANAGER => 'Менеджер',
//            \app\models\User::ROLE_USER => 'Пользователь',
//    ])
    ?>

    <?= $form->field($model, 'investing_plan')->textInput() ?>
    <?= $form->field($model, 'investing_confirmed')->textInput() ?>
    <?= $form->field($model, 'income_grow')->textInput() ?>
    <?= $form->field($model, 'income_reinvest')->textInput() ?>
    <?= $form->field($model, 'every_day_grow')->textInput() ?>
    <?= $form->field($model, 'every_hour_grow')->textInput() ?>
    <?= $form->field($model, 'common_grow')->textInput() ?>
    <?= $form->field($model, 'balance_income')->textInput() ?>
    <?= $form->field($model, 'balance_exchange')->textInput() ?>
    <?= $form->field($model, 'balance_withdraw')->textInput() ?>
    <?= $form->field($model, 'is_fake')->checkbox() ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'balance_usd')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'balance_eur')->textInput() ?>
        </div>
    </div>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
