<?php
use app\models\forms\AvatarForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */
/* @var $content string */

//$isGuest = Yii::$app->user->isGuest;
//if($isGuest)
//    $theme = 'default';
//else
//    $theme = Yii::$app->user->identity->theme;


if (Yii::$app->controller->action->id === 'login') { 
/**
 * Do not use this code in your template. Remove it. 
 * Instead, use the code  $this->layout = '//main-login'; in your controller.
 */
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else {

    if (class_exists('backend\assets\AppAsset')) {
        backend\assets\AppAsset::register($this);
    } else {
        app\assets\AppAsset::register($this);
    }

    app\assets\ColorAdminAsset::register($this);

    // dmstr\web\AdminLteAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <!-- ================== BEGIN BASE CSS STYLE ================== -->
        <?=$this->head()?>
        <link href="/theme/assets/css/theme/default.css" rel="stylesheet">
        <!-- ================== END BASE CSS STYLE ================== -->
        <script src="/theme/assets/plugins/jquery/jquery-1.9.1.min.js"></script>
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
<!--        <script src="/assets/pace/pace.min.js"></script>-->
    </head>
    <body class="bg-theme bg-theme1">

    <?php $this->beginBody() ?>

    <div id="pageloader-overlay" class="visible incoming" style="display: none;"><div class="loader-wrapper-outer"><div class="loader-wrapper-inner"><div class="loader"></div></div></div></div>

    <!-- start loader -->
<!--    <div id="pageloader-overlay" class="visible incoming"><div class="loader-wrapper-outer"><div class="loader-wrapper-inner"><div class="loader"></div></div></div></div>-->
    <!-- end loader -->

    <div id="wrapper">

        <?= $this->render(
            'header_menu.php',
            ['directoryAsset' => $directoryAsset]
        )?>

        <?= $this->render(
            'header.php',
            ['directoryAsset' => $directoryAsset]
        ) ?>

        <div class="clearfix"></div>


        <div class="content-wrapper" style="height: 100vh;">
            <div class="container-fluid">
                <?= $this->render(
                    'content.php',
                    ['content' => $content, 'directoryAsset' => $directoryAsset]
                ) ?>


                <div class="overlay toggle-menu"></div>
                <!--end overlay-->
            </div>
            <!-- End container-fluid-->
        </div>

        <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>

        <footer class="footer">
            <div class="container">
                <div class="text-center">
                    Copyright © 2020 Medici Investment Pro | ver. 0.1
                </div>
            </div>
        </footer>

    </div>





    <div class="hidden">
        <?php $avatar = new AvatarForm(); $avatarForm = ActiveForm::begin(['id' => 'avatar-form', 'action' => ['user/upload-avatar'], 'options' => ['enctype' => 'multipart/form-data']])  ?>

        <?= $avatarForm->field($avatar, 'file')->fileInput() ?>

        <?php ActiveForm::end() ?>
    </div>


    <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>

