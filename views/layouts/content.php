<?php

use yii\widgets\Breadcrumbs;
use yii\widgets\Pjax;
use app\models\Ticket;

$preContent = "";

if(Yii::$app->user->isGuest == false){
    if(Yii::$app->user->identity->isSuperAdmin()){
        $tickets = Ticket::find()->where(['notified' => false])->all();
        foreach ($tickets as $ticket){
            $preContent .= '
            <div class="alert alert-info alert-dismissible" role="alert">
		   <button type="button" class="close" data-dismiss="alert">×</button>
			<div class="alert-icon">
			 <i class="fa fa-bell"></i>
			</div>
			<div class="alert-message">
			  <span><strong>Новый тикет!</strong> Был создан новый тикет под наименованием «'.$ticket->subject.'»</span>
			</div>
		  </div>
            ';

            $ticket->notified = true;

            $ticket->save(false);

        }
    }
}

if(Yii::$app->session->hasFlash('info')){
    $preContent .= '
            <div class="alert alert-info alert-dismissible" role="alert">
		   <button type="button" class="close" data-dismiss="alert">×</button>
			<div class="alert-icon">
			 <i class="fa fa-bell"></i>
			</div>
			<div class="alert-message">
			  <span><strong>Внимание!</strong> '.Yii::$app->session->getFlash('info').'</span>
			</div>
		  </div>
            ';
}

?>
        <?= $preContent ?>

        <?= $content ?>
