<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

//$isGuest = Yii::$app->user->isGuest;
//if($isGuest)
//    $theme = 'default';
//else
//    $theme = Yii::$app->user->identity->theme;


\app\assets\ColorAdminAsset::register($this);

?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <!-- ================== BEGIN BASE CSS STYLE ================== -->
        <?=$this->head()?>
        <link href="/theme/assets/css/theme/default.css" rel="stylesheet">
        <!-- ================== END BASE CSS STYLE ================== -->
        <script src="/theme/assets/plugins/jquery/jquery-1.9.1.min.js"></script>
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <!--        <script src="/assets/pace/pace.min.js"></script>-->
    </head>
    <body class="bg-theme bg-theme1">

    <?php $this->beginBody() ?>

    <!-- start loader -->
    <!--    <div id="pageloader-overlay" class="visible incoming"><div class="loader-wrapper-outer"><div class="loader-wrapper-inner"><div class="loader"></div></div></div></div>-->
    <!-- end loader -->

    <div id="wrapper">


        <?= $this->render(
            'content.php',
            ['content' => $content,]
        ) ?>


        <a href="javaScript:void();" class="back-to-top"><i class="fa fa-angle-double-up"></i> </a>

    </div>







    <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>

