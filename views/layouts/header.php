<?php

use yii\helpers\Html;
use app\models\Users;
use yii\helpers\Url;

?>

<!--Start topbar header-->
<header class="topbar-nav">
    <nav id="header-setting" class="navbar navbar-expand fixed-top">
        <ul class="navbar-nav mr-auto align-items-center">
            <li class="nav-item">
                <a class="nav-link toggle-menu" href="javascript:void();">
                    <i class="icon-menu menu-icon"></i>
                </a>
            </li>
        </ul>

        <ul class="navbar-nav align-items-center right-nav-link">
            <li class="nav-item">
                <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" data-toggle="dropdown" href="#">
                    <span class="user-profile"><img src="/<?= Yii::$app->user->identity->realAvatar ?>" data-role="avatar-view" style="object-fit: contain;" class="img-circle" alt="user avatar"></span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li class="dropdown-item user-details">
                        <a href="<?= Url::toRoute(['user/profile']) ?>">
                            <div class="media">
                                <div class="avatar"><img class="align-self-start mr-3" src="/<?= Yii::$app->user->identity->realAvatar ?>" data-role="avatar-view" style="object-fit: contain;" alt="user avatar"></div>
                                <div class="media-body">
                                    <h6 class="mt-2 user-title"><?= Yii::$app->user->identity->name ?></h6>
                                    <p class="user-subtitle"><?= Yii::$app->user->identity->login ?></p>
                                </div>
                            </div>
                        </a>
                    </li>
<!--                    <li class="dropdown-divider"></li>-->
<!--                    <li class="dropdown-item"><i class="icon-envelope mr-2"></i> Inbox</li>-->
<!--                    <li class="dropdown-divider"></li>-->
<!--                    <li class="dropdown-item"><i class="icon-wallet mr-2"></i> Account</li>-->
<!--                    <li class="dropdown-divider"></li>-->
<!--                    <li class="dropdown-item"><i class="icon-settings mr-2"></i> Setting</li>-->
<!--                    <li class="dropdown-divider"></li>-->
                    <li class="dropdown-item"><a href="<?= \yii\helpers\Url::toRoute(['site/logout']) ?>" data-method="POST"><i class="icon-power mr-2"></i> Logout</a></li>
                </ul>
            </li>
        </ul>
    </nav>
</header>
<!--End topbar header-->