<?php

use yii\helpers\Url;

?>

<!--Start sidebar-wrapper-->
<div id="sidebar-wrapper" data-simplebar="" data-simplebar-auto-hide="true">
    <div class="brand-logo">
        <a href="index.html">
            <img src="/img/logo.png" style="height: 40px; width: unset;" class="logo-icon" alt="logo icon">
        </a>
    </div>
<!--    <div class="user-details">-->
<!--        <div class="media align-items-center user-pointer collapsed" data-toggle="collapse" data-target="#user-dropdown">-->
<!--            <div class="avatar"><img class="mr-3 side-user-img" src="https://via.placeholder.com/110x110" alt="user avatar"></div>-->
<!--            <div class="media-body">-->
<!--                <h6 class="side-user-name"></h6>-->
<!--            </div>-->
<!--        </div>-->
<!--        <div id="user-dropdown" class="collapse">-->
<!--            <ul class="user-setting-menu">-->
<!--                <li><a href="javaScript:void();"><i class="icon-user"></i>  My Profile</a></li>-->
<!--                <li><a href="javaScript:void();"><i class="icon-settings"></i> Setting</a></li>-->
<!--                <li><a href="javaScript:void();"><i class="icon-power"></i> Logout</a></li>-->
<!--            </ul>-->
<!--        </div>-->
<!--    </div>-->
    <ul class="sidebar-menu">
        <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>

            <li>
                <a href="<?= Url::toRoute(['/user']) ?>" class="waves-effect">
                    <i class="zmdi zmdi-accounts"></i> <span>Пользователи</span>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['/dashboard/company-statistic']) ?>" class="waves-effect">
                    <i class="zmdi zmdi-accounts"></i> <span>Статистика компании</span>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['/order']) ?>" class="waves-effect">
                    <i class="zmdi zmdi-flag"></i> <span>Заявки</span>
                </a>
            </li>
            <li style="display: none;">
                <a href="<?= Url::toRoute(['/transaction-in']) ?>" class="waves-effect">
                    <i class="zmdi zmdi-plus-square"></i> <span>Пополнение</span>
                </a>
            </li>
            <li style="display: none;">
                <a href="<?= Url::toRoute(['/transaction-out']) ?>" class="waves-effect">
                    <i class="zmdi zmdi-minus-square"></i> <span>Вывод</span>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['/ref-agent']) ?>" class="waves-effect">
                    <i class="zmdi zmdi-accounts-list"></i> <span>Агенты</span>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['/ticket']) ?>" class="waves-effect">
                    <i class="zmdi zmdi-alert-triangle"></i> <span>Тикеты</span>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['/news']) ?>" class="waves-effect">
                    <i class="zmdi zmdi-tv-list"></i> <span>Новости</span>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['/faq']) ?>" class="waves-effect">
                    <i class="zmdi zmdi-help"></i> <span>ЧАВО</span>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['/log']) ?>" class="waves-effect">
                    <i class="zmdi zmdi-book"></i> <span>Логи</span>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['/settings']) ?>" class="waves-effect">
                    <i class="zmdi zmdi-book"></i> <span>Настройки</span>
                </a>
            </li>
        <?php else: ?>
            <li>
                <a href="<?= Url::toRoute(['/dashboard/index']) ?>" class="waves-effect">
                    <i class="zmdi zmdi-accounts"></i> <span>Личный профиль</span>
                </a>
            </li>
            <li style="display: none;">
                <a href="<?= Url::toRoute(['/dashboard/investing']) ?>" class="waves-effect">
                    <i class="zmdi zmdi-long-arrow-up"></i> <span>Управление активом</span>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['/dashboard/statistic']) ?>" class="waves-effect">
                    <i class="zmdi zmdi-chart"></i> <span>Статистика</span>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['/ref-agent/index']) ?>" class="waves-effect">
                    <i class="zmdi zmdi-accounts-list"></i> <span>Реф. система</span>
                </a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['/ticket/create']) ?>" class="waves-effect">
                    <i class="zmdi zmdi-help"></i> <span>Тех поддержка</span>
                </a>
            </li>
            <li style="display: none;">
                <a href="<?= Url::toRoute(['/dashboard/account']) ?>" class="waves-effect">
                    <i class="zmdi zmdi-balance-wallet"></i> <span>Счет</span>
                </a>
            </li>
        <?php endif; ?>
    </ul>

</div>
<!--End sidebar-wrapper-->