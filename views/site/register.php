<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Регистрация';

$fieldOptions1 = [
    'options' => ['class' => 'form-group m-b-20'],
    'inputTemplate' => "{input}"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group m-b-20'],
    'inputTemplate' => "{input}"
];

$ref = null;
if(isset($_GET['ref_id'])){
//    $ref = \app\models\RefAgent::findOne($_GET['ref_id']);
    $ref = $_GET['ref_id'];
}

if($ref){
//    $model->email = $ref->email;
    $model->ref_id = $ref;
}


?>

<div class="card card-authentication1 mx-auto my-5">
    <div class="card-body">
        <div class="card-content p-2">
            <div class="text-center">
                <img src="/img/logo.png" alt="logo icon">
            </div>
            <div class="card-title text-uppercase text-center py-3">Зарегистрироваться</div>
            <?php $form = ActiveForm::begin(['id' => 'register-form', 'enableClientValidation' => false], $options = ['class' => 'margin-bottom-0']); ?>
            <div class="row">
                <div class="col-md-12">
                    <?= $form
                        ->field($model, 'email', $fieldOptions1)
                        ->label(false)
                        ->textInput(['placeholder' => $model->getAttributeLabel('email'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>
                </div>
            </div>
            <div class="row">
                <div id="phone-wrapper" class="col-md-12">
                    <?= $form
                        ->field($model, 'phone', $fieldOptions1)
                        ->label(false)
                        ->textInput(['placeholder' => $model->getAttributeLabel('phone'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>
                </div>
            </div>
            <?= $form
                ->field($model, 'password', $fieldOptions2)
                ->label(false)
                ->passwordInput(['placeholder' => $model->getAttributeLabel('password'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>
            <?= $form
                ->field($model, 'repeatPassword', $fieldOptions2)
                ->label(false)
                ->passwordInput(['placeholder' => $model->getAttributeLabel('repeatPassword'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>
            <div class="hidden">
                <?= $form->field($model, 'ref_id')->hiddenInput()->label(false) ?>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- добавление элемента div -->
                    <div class="g-recaptcha" data-sitekey="6LdqyAAaAAAAABzixqd18lqfoxW-4EiFbLAZRAft"></div>

                    <!-- элемент для вывода ошибок -->
                    <div class="text-danger" id="recaptchaError"></div>
                </div>
            </div>
            <div class="login-buttons">
                <?= Html::submitButton('Регистрация', ['class' => 'btn btn-success btn-block btn-lg', 'name' => 'login-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>



<?php

$script = <<< JS

$('#sms-button').click(function(event){
    event.preventDefault();
    var email = $('#signupform-email').val();

    var btn = this;
    
    $.get('/site/send-sms?email='+email, function(response){
        alert('Код отправлен');
        $('#phone-wrapper').removeClass('col-md-7');
        $('#phone-wrapper').addClass('col-md-12');
        $('#sms-wrapper').addClass('hidden');
    });
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>

