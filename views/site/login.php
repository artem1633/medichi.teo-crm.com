<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Авторизация';

$fieldOptions1 = [
    'options' => ['class' => 'form-group m-b-20'],
    'inputTemplate' => "{input}"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group m-b-20'],
    'inputTemplate' => "{input}"
];
?>

<div class="card card-authentication1 mx-auto my-5">
    <div class="card-body">
        <div class="card-content p-2">
            <div class="text-center">
                <img src="/img/logo.png" alt="logo icon">
            </div>
            <div class="card-title text-uppercase text-center py-3">Войти</div>
            <?php if(Yii::$app->session->hasFlash('success')): ?>
                <p class="text-success"><?= Yii::$app->session->getFlash('success') ?></p>
            <?php endif; ?>
            <?php if(Yii::$app->session->hasFlash('error')): ?>
                <p class="text-danger"><?= Yii::$app->session->getFlash('error') ?></p>
            <?php endif; ?>
            <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false], $options = ['class' => 'margin-bottom-0']); ?>
            <?= $form
                ->field($model, 'username', $fieldOptions1)
                ->label(false)
                ->textInput(['placeholder' => $model->getAttributeLabel('email'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>
            <?= $form
                ->field($model, 'password', $fieldOptions2)
                ->label(false)
                ->passwordInput(['placeholder' => $model->getAttributeLabel('password'), 'class' => 'form-control input-lg inverse-mode no-border']) ?>
            <div class="row">
                <div class="col-md-12">
                    <!-- добавление элемента div -->
                    <div class="g-recaptcha" data-sitekey="6LdqyAAaAAAAABzixqd18lqfoxW-4EiFbLAZRAft"></div>

                    <!-- элемент для вывода ошибок -->
                    <div class="text-danger" id="recaptchaError"></div>
                </div>
            </div>
            <div class="login-buttons" style="margin-top: 10px;">
                <?= Html::submitButton('Войти', ['class' => 'btn btn-success btn-block btn-lg', 'name' => 'login-button']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
    <div class="card-footer text-center py-3">
        <p class="mb-0"><?= Html::a('Востановить пароль', ['site/forget-password'], ['class' => 'pull-left']) ?> <?= Html::a('Зарегистрироваться', ['site/register'], ['class' => 'pull-right']) ?></p>
    </div>
</div>



<?php

$script = <<< JS

$('#sms-button').click(function(event){
    event.preventDefault();
    var username = $('#loginform-username').val();

    var btn = this;
    
    $.get('/site/send-login-sms?username='+username, function(response){
        alert('Код отправлен');
        $('#phone-wrapper').removeClass('col-md-7');
        $('#phone-wrapper').addClass('col-md-12');
        $('#sms-wrapper').addClass('hidden');
    });
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>

