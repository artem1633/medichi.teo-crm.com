<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Log */
?>
<div class="log-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'content:ntext',
            'created_at',
        ],
    ]) ?>

</div>
