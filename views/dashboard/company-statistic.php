<?php

use app\models\Order;
use app\models\User;
use yii\helpers\ArrayHelper;

/** @var $this yii\web\View */

$this->title = 'Стастика компании';

?>

<div class="row pt-2 pb-2">
    <div class="col-sm-9">
        <h5 class="page-title">Стастика компании</h5>
        <hr>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h6 class="text-uppercase" style="margin-top: 15px;">Компания заработала</h6>
        <hr>
    </div>
</div>
<div class="card mt-3">
    <div class="card-content">
        <div class="row row-group m-0">
            <div class="col-12 col-lg-6 col-xl-4 border-light">
                <div class="card-body">
                    <p>Всего</p>
                    <h4 class="mb-0">120% (1200 USD)</h4>
                </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-4 border-light">
                <div class="card-body">
                    <p>За месяц</p>
                    <h4 class="mb-0">30% (300 USD)</h4>
                </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-4 border-light">
                <div class="card-body">
                    <p>За сутки</p>
                    <h4 class="mb-0">12% (120 USD)</h4>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h6 class="text-uppercase">Инвесторов в Medici:</h6>
        <hr>
    </div>
</div>
<div class="card mt-3">
    <div class="card-content">
        <div class="row row-group m-0">
            <div class="col-12 col-lg-6 col-xl-12 border-light">
                <div class="card-body">
                    <p>Всего пользователей</p>
                    <h4 class="mb-0"><?= User::find()->count(); ?></h4>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h6 class="text-uppercase">Объем инвестированных средств</h6>
        <hr>
    </div>
</div>
<div class="card mt-3">
    <div class="card-content">
        <div class="row row-group m-0">
            <div class="col-12 col-lg-6 col-xl-4 border-light">
                <div class="card-body">
                    <p>Всего</p>
                    <?php

                    $ordersSum = number_format(Order::find()->where(['type' => Order::TYPE_REPLENISHMENT])->sum('value'), 0, '.', ' ');

                    ?>
                    <h4 class="mb-0"><?= $ordersSum ?> USD</h4>
                </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-4 border-light">
                <div class="card-body">
                    <p>За месяц</p>
                    <?php

                    $start = date('Y-m-').'01 00:00:00';
                    $end = date('Y-m-t ').'23:59:59';

                    $ordersSum = number_format(Order::find()->where(['type' => Order::TYPE_REPLENISHMENT])->andWhere(['between', 'created_at', $start, $end])->sum('value'), 0, '.', ' ');

                    ?>
                    <h4 class="mb-0"><?=$ordersSum?> USD</h4>
                </div>
            </div>
            <div class="col-12 col-lg-6 col-xl-4 border-light">
                <div class="card-body">
                    <p>За сутки</p>
                    <?php

                    $start = date('Y-m-d ').'00:00:00';
                    $end = date('Y-m-d ').'23:59:59';

                    $ordersSum = number_format(Order::find()->where(['type' => Order::TYPE_REPLENISHMENT])->andWhere(['between', 'created_at', $start, $end])->sum('value'), 0, '.', ' ');

                    ?>
                    <h4 class="mb-0"><?= $ordersSum ?> USD</h4>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h6 class="text-uppercase">Последняя транзакция</h6>
        <hr>
    </div>
</div>
<div class="card mt-3">
    <div class="card-content">
        <div class="row row-group m-0">
            <div class="col-12 col-lg-6 col-xl-12 border-light">
                <div class="card-body">
                    <?php

                    $lastOrder = Order::find()->orderBy('id desc')->where(['is not','type',null])->one();

                    ?>
                    <h5 class="mb-0"><b><?=ArrayHelper::getValue($lastOrder, 'user.login')?> в <?=date('d.m.Y H:i', strtotime($lastOrder->created_at)); ?>.  выполнена операция (<?= ArrayHelper::getValue(Order::typeLabels(), $lastOrder->type) ?>) на сумму </b> <?= $lastOrder->value ?> USD</h5>
                </div>
            </div>
        </div>
    </div>
</div>
