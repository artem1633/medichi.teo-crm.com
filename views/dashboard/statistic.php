<?php

/** @var $this yii\web\View */


use app\models\Order;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\models\User;
use app\models\Settings;

$this->title = 'Статистика';

$companyAll = floatval(Settings::findByKey('company_money_all')->value);
$companyMonth = floatval(Settings::findByKey('company_money_month')->value);
$companyDay = floatval(Settings::findByKey('company_money_day')->value);

?>

<div class="row pt-2 pb-2">
    <div class="col-sm-9">
        <h4 class="page-title">Статистика</h4>
        <hr>
    </div>
</div>


<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="media align-items-center">
                    <div class="media-body">
                        <h5 class="mb-0"><?= User::find()->where(['is_fake' => false])->count(); ?></h5>
                        <p class="mb-0">Инвесторов в Medici</p>
                    </div>
                </div>
                <div class="progress-wrapper mt-3">
                    <div class="progress mb-0" style="height: 5px;">
                        <div class="progress-bar bg-white" role="progressbar" style="width: 45%"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="media align-items-center">
                    <div class="media-body">
                        <h5 class="mb-0"><h4 class="mb-0">5% <?= Yii::$app->user->identity->commonIncomeTotal() ?> USD, <?= Yii::$app->user->identity->delegateIncomeMdcAll() ?> MDC </h4></h5>
                        <p class="mb-0">Компания заработала</p>
                    </div>
                </div>
                <div class="progress-wrapper mt-3">
                    <div class="progress mb-0" style="height: 5px;">
                        <div class="progress-bar bg-white" role="progressbar" style="width: 45%"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- 
    <div class="row">
        <div class="col-md-12">
            <h6 class="text-uppercase" style="margin-top: 25px;">Компания заработала</h6>
            <hr style="margin-bottom: 0;">
        </div>
    </div>
    <div class="card mt-3">
        <div class="card-content">
            <div class="row row-group m-0">
                <div class="col-12 col-lg-6 col-xl-4 border-light">
                    <div class="card-body">
                        <p>Всего</p>
                        <h4 class="mb-0">5% <?= Yii::$app->user->identity->commonIncomeTotal() ?> USD, <?= Yii::$app->user->identity->delegateIncomeMdcAll() ?> MDC </h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
 -->




    <div class="row">
        <div class="col-md-12">
            <h6 class="text-uppercase" style="margin-top: 25px;">Объем инвестированных средств</h6>
            <hr style="margin-bottom: 0;">
        </div>
    </div>
    <div class="card mt-3">
        <div class="card-content">
            <div class="row row-group m-0">
                <?php

                $all = number_format(Order::find()->where(['type' => Order::TYPE_REPLENISHMENT])->sum('value'), 0, '.', ',');
                $month = number_format(Order::find()->where(['type' => Order::TYPE_REPLENISHMENT])->andWhere(['>', 'created_at', date('Y-m-d H:i:s', time() - 2.628e+6)])->sum('value'), 0, '.', ',');
                $day = number_format(Order::find()->where(['type' => Order::TYPE_REPLENISHMENT])->andWhere(['>', 'created_at', date('Y-m-d H:i:s', time() - 86400)])->sum('value'), 0, '.', ',');

                ?>
                <div class="col-12 col-lg-6 col-xl-4 border-light">
                    <div class="card-body">
                        <p>Всего</p>
                        <h4 class="mb-0"><?=$all?> USD</h4>
                    </div>
                </div>
                <div class="col-12 col-lg-6 col-xl-4 border-light">
                    <div class="card-body">
                        <p>За месяц</p>
                        <h4 class="mb-0"><?=$month?> USD</h4>
                    </div>
                </div>
                <div class="col-12 col-lg-6 col-xl-4 border-light">
                    <div class="card-body">
                        <p>За сутки</p>
                        <h4 class="mb-0"><?= $day ?> USD</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h6 class="text-uppercase" style="margin-top: 25px;">Последняя транзакция</h6>
            <hr style="margin-bottom: 0;">
        </div>
    </div>
    <div class="card mt-3">
        <div class="card-content">
            <div class="row row-group m-0">
                <div class="col-12 col-lg-6 col-xl-12 border-light">
                    <div class="card-body">
                        <?php

                        $lastOrder = Order::find()->orderBy('id desc')->where(['is not','type',null])->one();

                        ?>
                        <h5 class="mb-0"><b><?=date('d.m.Y H:i', strtotime($lastOrder->created_at)); ?> выполнена операция (<?= ArrayHelper::getValue(Order::typeLabels(), $lastOrder->type) ?>) на сумму </b> <?= $lastOrder->value ?> USD</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>








<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.min.js"></script>

<script>
//     var ctx = document.getElementById('charContent').getContext('2d');
//     var charContent = new Chart(ctx, {
//       	type: 'line',
//       	data: {
//         	labels: ['01-Март', '02-Март', '03-Март', '04-Март', '05-Март', '06-Март', '07-Март'],
//         	datasets: [{
//           		label: 'Сумма',
//           		data: [120, 190, 300, 170, 600, 340, 450],
//           		backgroundColor: "rgba(153,255,51,0.6)"
//         	}]
//       	}
//     });



</script>

<?php

$script = <<< JS
var options = {
    chart: {
        height: 125,
        type: 'area',
        zoom: {
            enabled: false
        },
        foreColor: 'rgba(255, 255, 255, 0.65)',
        toolbar: {
            show: false
        },
        sparkline:{
            enabled:true,
        },
        dropShadow: {
            enabled: false,
            opacity: 0.15,
            blur: 3,
            left: -7,
            top: 15,
            //color: 'rgba(0, 158, 253, 0.65)',
        }
    },
    plotOptions: {
        bar: {
            columnWidth: '10%',
            endingShape: 'rounded',
            dataLabels: {
                position: 'top', // top, center, bottom
            },
        }
    },
    dataLabels: {
        enabled: false
    },
    stroke: {
        width: 3,
        curve: 'straight'
    },
    series: [{
        name: 'Заработано',
        data: [76, 85, 101, 98, 87, 105, 91, 114, 94, 76, 85, 101, 98, 87, 105, 91, 114, 94]
    }],

    xaxis: {
        type: 'month',
        categories: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct"],
    },
    yaxis: {
        axisBorder: {
            show: false
        },
        axisTicks: {
            show: false,
        },
        labels: {
            show: false,
            formatter: function(val) {
                return parseInt(val);
            }
        }

    },
    fill: {
        type: 'gradient',
        gradient: {
            shade: 'light',
            //gradientToColors: ['rgba(255, 255, 255, 0.12)'],
            shadeIntensity: 1,
            type: 'vertical',
            opacityFrom: 0.7,
            opacityTo: 0.1,
            stops: [0, 100, 100, 100]
        },
    },
    colors: ["#fff"],
    legend: {
        show: 0,
        position: "top",
        horizontalAlign: "center",
        offsetX: -20,
        fontSize: "12px",
        markers: {
            radius: 50,
            width: 10,
            height: 10
        }
    },
    grid:{
        show: false,
        borderColor: 'rgba(66, 59, 116, 0.12)',
    },
    tooltip: {
        theme: 'dark',
        x: {
            show: false
        },

    }
};

var chart = new ApexCharts(
    document.querySelector("#twitter-followers"),
    options
);

chart.render();
JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>