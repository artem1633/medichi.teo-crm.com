<?php

/** @var $this yii\web\View */


use app\models\Order;
use app\models\Settings;
use app\models\Transaction;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Главная';

/** @var \app\models\User $user */
$user = Yii::$app->user->identity;

$pmAccount = Settings::findByKey('pm_payee_account')->value;
$pmUnit = Settings::findByKey('pm_payment_units')->value;

$mdcPrice = floatval(\app\models\Settings::findByKey('mdc_price')->value);

?>

<div class="row pt-2 pb-2">
    <div class="col-sm-9">
        <h4 class="page-title">Личный профиль</h4>
        <hr>
    </div>
</div>

                <p>На этой странице Вы можете управлять своими инвестиционными активами.
Любые вопросы Вы можете задать нашим специалистам на вкладке «Тех. Поддержка», а также любым другим удобным для Вас способом, указанном на сайте.
</p>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title">Пополнение
                    <div class="card-action">
                    </div>
                </div>
                <p>Чтобы начать инвестировать, Вам необходимо пополнить общий баланс в системе.
Доступно автоматическое пополнение через платёжную систему Perfect Money, а также переводом на карту через администратора .
</p>
                <form id="perfectmoney-form" action="https://perfectmoney.com/api/step1.asp" method="POST">
                    <input type="hidden" name="PAYEE_ACCOUNT" value="<?=$pmAccount?>">
                    <input type="hidden" name="PAYEE_NAME" value="http://medichi.teo-crm.com/">
                    <input type="hidden" name="PAYMENT_ID" value="">
                    <input type="hidden" name="PAYMENT_UNITS" value="<?=$pmUnit?>">
                    <input type="hidden" name="STATUS_URL" value="">
                    <input type="hidden" name="PAYMENT_URL" value="http://medichi.teo-crm.com/dashboard/transaction-success?userId=<?= Yii::$app->user->getId() ?>&amount=">
                    <input type="hidden" name="PAYMENT_URL_METHOD" value="LINK">
                    <input type="hidden" name="NOPAYMENT_URL" value="http://medichi.teo-crm.com/dashboard/fuck">
                    <input type="hidden" name="NOPAYMENT_URL_METHOD" value="LINK">
                    <input type="hidden" name="SUGGESTED_MEMO" value="">
                    <input type="hidden" name="BAGGAGE_FIELDS" value="">
                    <div class="input-group" style="width: 35%;">
                        <div class="input-group-prepend">
                            <span class="input-group-text">$</span>
                        </div>
                        <input class="form-control" style="width: 15%; display: inline-block; float: left;" placeholder="Введите сумму в USD" type="text" name="PAYMENT_AMOUNT" value="" onchange="$('input[name=\'PAYMENT_URL\']').val('http://medichi.teo-crm.com/dashboard/transaction-success?userId=<?= Yii::$app->user->getId() ?>&amount='+value);">
                        <div class="input-group-append">
                            <input id="payment-btn" class="btn btn-light" type="submit" name="PAYMENT_METHOD" value="Perfect Money">
                            <?= Html::a('через Администратора', ['site/card-info'], ['class' => 'btn btn-light', 'style' => 'padding-top: 9px;', 'role' => 'modal-remote']) ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
<div class="row">
   
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="card-title">USD-кошелёк
                    <div class="card-action">
                        <?= Html::a('Вывести', ['transaction-out/new', 'currency' => Transaction::CURRENCY_USD], ['class' => 'btn btn-light btn-xs', 'style' => 'margin-top: -9px', 'role' => 'modal-remote']) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p>Ваш баланс в USD-кошельке</p>
                    </div>
                    <div class="col-md-12">
                        <p style="font-size: 20px; font-weight: 700;" class="text-success pull-right"><?= Yii::$app->user->identity->balance_usd ?> usd</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="card-title">MDC-кошелёк
                    <div class="card-action">
                        <?= Html::a('Вывести', ['transaction-out/new', 'currency' => Transaction::CURRENCY_MDC], ['class' => 'btn btn-light btn-xs', 'style' => 'margin-top: -9px', 'role' => 'modal-remote']) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p>Ваш баланс в MDC-кошельке</p>
                    </div>
                    <div class="col-md-12">
                        <p style="font-size: 20px; font-weight: 700;" class="text-success pull-right"><?= Yii::$app->user->identity->balance_eur ?> mdc (<?=Yii::$app->user->identity->balance_eur * $mdcPrice?> usd)</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="row">
       
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="card-title">Делегирование
                    <div class="card-action">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p>
                            Заделегируйте денежные средства с Вашего баланса – это даст Вам повышенный доход! При делегировании Ваши средства «замораживаются» для вывода. Вывод возможен по истечении 30 дней после разделегирования этих средств.
                        </p>
                    </div>

                    <div class="col-md-12">
                        <?php


                        $ordersSum = Order::find()->where(['user_id' => Yii::$app->user->getId(), 'type' => Order::TYPE_REPLENISHMENT])->sum('value');

                        $ordersSum = $ordersSum + ($mdcPrice * Yii::$app->user->identity->balance_eur);

                        ?>
                        <p style="font-size: 20px; font-weight: 700;" class="text-success pull-right"><?= $ordersSum ?> USD </p>
                    </div>
                </div>
                <hr>
            </div>
        </div>
    </div>
     <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="card-title">Ваша прибыль
                        <div class="card-action">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p>
                                Ваша прибыль в настоящий момент пересчитывается 2 раза в месяц
                            </p>
                        </div>

                        <div class="col-md-12">
                            <?php


                            $ordersSum = Order::find()->where(['user_id' => Yii::$app->user->getId(), 'type' => Order::TYPE_REPLENISHMENT])->sum('value');

                            $ordersSum = $ordersSum + ($mdcPrice * Yii::$app->user->identity->balance_eur);

                            ?>
                            <p style="font-size: 20px; font-weight: 700;" class="text-success pull-right"><?= $ordersSum ?> USD </p>
                        </div>
                    </div>
                    <hr>
                </div>
            </div>
        </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title">Доступно для вывода
                    <div class="card-action">
                    </div>
                </div>
                <p>Ваши средства, которые доступны для вывода на текущий момент.
                    Вы не можете моментально вывести заделегированные активы. Для этого их необходимо предварительно разделегировать
                </p>
                <hr>
            </div>
        </div>
    </div>
</div>

<?php

$id = Yii::$app->user->getId();

$script = <<< JS

$("#payment-btn").click(function(e){
    e.preventDefault();
    
    $.get("/order/generate-hash?id={$id}", function(response){
        var hash = response.result;
        var amount = $('input[name="PAYMENT_AMOUNT"]').val();
        
        $('input[name="PAYMENT_URL"]').val('http://medichi.teo-crm.com/dashboard/transaction-success?userId={$id}&amount='+amount+'&hash='+hash);
        
        $('#perfectmoney-form').submit();
    });
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>