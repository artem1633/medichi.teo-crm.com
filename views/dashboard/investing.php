<?php

/** @var $this yii\web\View */

use yii\helpers\Html;
use app\models\Order;

$this->title = 'Управление активом';

?>

<!--    <div class="row pt-2 pb-2">-->
<!--        <div class="col-sm-9">-->
<!--            <h4 class="page-title">Управление активом</h4>-->
<!--            <hr>-->
<!--        </div>-->
<!--    </div>-->

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="card-title">MDC
                    <div class="card-action">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <?= Html::input('text', '', null, ['class' => 'form-control']) ?>
                            <div class="input-group-append">
                                <?= Html::a('Купить', ['#'], ['class' => 'btn btn-light', 'data-new' => Order::TYPE_BUY, 'style' => 'padding-top: 9px;']) ?>
                                <?= Html::a('Продать', ['#'], ['class' => 'btn btn-light', 'data-new' => Order::TYPE_SELL, 'style' => 'padding-top: 9px;']) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="card-title">Делегировать mdc
                    <div class="card-action">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <?= Html::input('text', '', null, ['class' => 'form-control']) ?>
                            <div class="input-group-append">
                                <?= Html::a('Делегировать', ['#'], ['class' => 'btn btn-light', 'data-new' => Order::TYPE_DELEGATE_MDC, 'style' => 'padding-top: 9px;']) ?>
                                <?= Html::a('Ределегировать', ['#'], ['class' => 'btn btn-light', 'data-new' => Order::TYPE_REDELEGATE_MDC, 'style' => 'padding-top: 9px;']) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h6 class="text-uppercase">Инвестиционные проекты</h6>
        <hr>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="card-title">Инвестирование в $
                    <div class="card-action">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <?= Html::input('text', '', null, ['class' => 'form-control']) ?>
                            <div class="input-group-append">
                                <?= Html::a('Инвестирование в $', ['#'], ['class' => 'btn btn-light btn-primary', 'data-new' => Order::TYPE_INVEST_PROJECT, 'style' => 'padding-top: 9px;']) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="card-title">Автореинвестинг
                    <div class="card-action">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            <?= Html::input('text', '', null, ['class' => 'form-control']) ?>
                            <div class="input-group-append">
                                <?= Html::a('Да', ['#'], ['class' => 'btn btn-light', 'data-new' => Order::TYPE_REINVEST_PROJECT_YES, 'style' => 'padding-top: 9px;']) ?>
                                <?= Html::a('Нет', ['#'], ['class' => 'btn btn-light', 'data-new' => Order::TYPE_REINVEST_PROJECT_NO, 'style' => 'padding-top: 9px;']) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php

$account = \yii\helpers\Url::toRoute(['dashboard/account'], true);

$script = <<< JS

$('[data-new]').click(function(event){
    event.preventDefault();

    var type = $(this).data('new');
    var value = $(this).parent().parent().find('input').val();
    
    $.get('/order/new?type='+type+'&value='+value, function(response){
        if(response.result === true){
            alert('Заявка отправлена');
            window.location = '{$account}';
        } else if(response.result === false){
            alert('Во время отправки заявки произошла ошибка');
        }
    });
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>