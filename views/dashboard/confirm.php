<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RefAgent */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ref-agent-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'code')->textInput() ?>

    <?php ActiveForm::end(); ?>

</div>
