<?php

/** @var $this yii\web\View */


use app\models\Settings;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = 'Счет';

\johnitvn\ajaxcrud\CrudAsset::register($this);

$pmAccount = Settings::findByKey('pm_payee_account')->value;
$pmUnit = Settings::findByKey('pm_payment_units')->value;

?>

<?php Pjax::begin(['id' => 'pjax-account-container', 'enablePushState' => false]) ?>

<!--<iframe src="https://yoomoney.ru/quickpay/shop-widget?writer=seller&targets=%D0%9F%D0%BE%D0%BF%D0%BE%D0%BB%D0%BD%D0%B5%D0%BD%D0%B8%D0%B5%20%D0%BB%D0%B8%D1%87%D0%BD%D0%BE%D0%B3%D0%BE%20%D1%81%D1%87%D0%B5%D1%82%D0%B0&targets-hint=&default-sum=10000&button-text=11&payment-type-choice=on&hint=&successURL=&quickpay=shop&account=4100116047348588" width="410" height="238" frameborder="0" allowtransparency="true" scrolling="no"></iframe>-->

<div class="row pt-2 pb-2">
    <div class="col-sm-9">
        <h4 class="page-title">Счет</h4>
        <hr>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title">Пополнение
                    <div class="card-action">
                    </div>
                </div>
                <form action="https://perfectmoney.com/api/step1.asp" method="POST">
<!--                    <input type="hidden" name="PAYEE_ACCOUNT" value="U23752773">-->
                    <input type="hidden" name="PAYEE_ACCOUNT" value="<?=$pmAccount?>">
                    <input type="hidden" name="PAYEE_NAME" value="http://app.medici-invest.com/">
                    <input type="hidden" name="PAYMENT_ID" value="">
<!--                    <input type="hidden" name="PAYMENT_UNITS" value="USD">-->
                    <input type="hidden" name="PAYMENT_UNITS" value="<?=$pmUnit?>">
                    <input type="hidden" name="STATUS_URL" value="">
                    <input type="hidden" name="PAYMENT_URL" value="http://app.medici-invest.com/api/money/good">
                    <input type="hidden" name="PAYMENT_URL_METHOD" value="LINK">
                    <input type="hidden" name="NOPAYMENT_URL" value="http://app.medici-invest.com/api/money/fuck">
                    <input type="hidden" name="NOPAYMENT_URL_METHOD" value="LINK">
                    <input type="hidden" name="SUGGESTED_MEMO" value="">
                    <input type="hidden" name="BAGGAGE_FIELDS" value="">
                    <div class="input-group" style="width: 35%;">
                        <div class="input-group-prepend">
                            <span class="input-group-text">$</span>
                        </div>
                        <input class="form-control" style="width: 15%; display: inline-block; float: left;" placeholder="Введите сумму в USD" type="text" name="PAYMENT_AMOUNT" value="">
                        <div class="input-group-append">
                            <input class="btn btn-light" type="submit" name="PAYMENT_METHOD" value="Пополнение">
                            <?= Html::a('Пополнить на карту', ['site/card-info'], ['class' => 'btn btn-light', 'style' => 'padding-top: 9px;', 'role' => 'modal-remote']) ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="card-title">Счет в mdc
                    <div class="card-action">
                        <?= Html::a('Вывести', ['transaction-out/new'], ['class' => 'btn btn-light btn-xs', 'style' => 'margin-top: -9px', 'role' => 'modal-remote']) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p>Ваш счет в mdc в данный момент</p>
                    </div>
                    <div class="col-md-12">
                        <p style="font-size: 20px; font-weight: 700;" class="text-success pull-right"><?= Yii::$app->user->identity->balance_eur ?> mdc</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="card-title">Счет в USD
                    <div class="card-action">
                        <?= Html::a('Вывести', ['transaction-out/new'], ['class' => 'btn btn-light btn-xs', 'style' => 'margin-top: -9px', 'role' => 'modal-remote']) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p>Ваш счет в usd в данный момент</p>
                    </div>
                    <div class="col-md-12">
                        <p style="font-size: 20px; font-weight: 700;" class="text-success pull-right"><?= Yii::$app->user->identity->balance_rub ?> usd</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php Pjax::end() ?>


<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
