<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TicketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Тикеты";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>

<div class="row pt-2 pb-2">
    <div class="col-sm-9">
        <h5 class="page-title">Тикеты</h5>
        <hr>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <?=GridView::widget([
                    'id'=>'crud-datatable',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'pjax'=>true,
                    'columns' => require(__DIR__.'/_columns.php'),
                    'panelBeforeTemplate' =>    Html::a('Добавить <i class="fa fa-plus"></i>', ['create'],
                            ['role'=>'modal-remote','title'=> 'Добавить тикет','class'=>'btn btn-success']).'&nbsp;'.
                        Html::a('<i class="fa fa-repeat"></i>', [''],
                            ['data-pjax'=>1, 'class'=>'btn btn-white', 'title'=>'Обновить']),
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'panel' => [
                        'headingOptions' => ['style' => 'display: none;'],
                        'after'=>''.
                            '<div class="clearfix"></div>',
                    ]
                ])?>
            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<?php

$script = <<< JS
new SimpleBar(document.getElementById('crud-datatable-container'));
JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>