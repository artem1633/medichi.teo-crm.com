<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Ticket */
/* @var $form yii\widgets\ActiveForm */
?>
<p>Мы всегда готовы ответить на любые Ваши вопросы</p>
<div class="ticket-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
        <?= $form->field($model, 'user_id')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\User::find()->all(), 'id', 'login')) ?>
    <?php endif; ?>

    <?= $form->field($model, 'subject')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div style="display: none;">
        <?= $form->field($model, 'file')->fileInput() ?>
    </div>

    <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
        <?= $form->field($model, 'status')->dropDownList(\app\models\Ticket::getStatuses()) ?>
    <?php endif; ?>

	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <div style="display: none;">
                <?= Html::a('Прикрепить файл', '#', ['id' => 'btn-file', 'class' => 'btn btn-light']) ?>
            </div>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>

<?php

$script = <<< JS
$('#btn-file').click(function(e){
    e.preventDefault();
    
    $('#ticket-file').trigger('click');
});

$('#ticket-file').change(function(e){
    $('#btn-file').text('Файл выбран');
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>
