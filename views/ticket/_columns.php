<?php
use yii\helpers\Url;
use yii\helpers\Html;

return [
//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '20px',
//    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'label' => 'Пользователь',
        'attribute'=>'user.login',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'subject',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'description',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'content' => function($data){
            switch ($data->status){
                case \app\models\Ticket::STATUS_NEW:
                    return '<span class="label label-sm label-info">Новый</span>';
                    break;
                case \app\models\Ticket::STATUS_WORK:
                    return '<span class="label label-primary">В работе</span>';
                    break;
                case \app\models\Ticket::STATUS_DONE:
                    return '<span class="label label-success">Выполнено</span>';
                    break;
                case \app\models\Ticket::STATUS_REJECTED:
                    return '<span class="label label-danger">Отклонен</span>';
                    break;
            }
        },
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
        'width' => '100px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'is_read',
        'hAlign' => \kartik\grid\GridView::ALIGN_CENTER,
        'content' => function($data){
            if($data->is_read == 0){
                return '<i class="fa fa-times text-danger"></i>';
            } else {
                return '<i class="fa fa-check text-success"></i>';
            }
        },
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
        'width' => '100px',
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'last_message_datetime',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_at',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'closed_datetime',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template' => '{view}&nbsp;{update}',
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Удалить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Вы уверены?',
                    'data-confirm-message'=>'Вы действительно хотите удалить данную запись?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Изменить',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";
            }
        ],
    ],

];   