<?php

namespace app\controllers;

use app\models\forms\ForgetPassword;
use app\models\forms\ResetPasswordForm;
use app\models\forms\SignupForm;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\forms\LoginForm;
use yii\helpers\Html;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Для изменения пароля
     * @return array
     */
    public function actionResetPassword()
    {
        $request = Yii::$app->request;
        $user = Yii::$app->user->identity;
        $model = new ResetPasswordForm(['uid' => $user->id]);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet) {
                return [
                    'title' => "Сменить пароль",
                    'content' => $this->renderAjax('reset-password-form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-white pull-left btn-sm', 'data-dismiss' => "modal"]) .
                        Html::button('Изменить', ['class' => 'btn btn-primary btn-sm', 'type' => "submit"])

                ];
            } else if($model->load($request->post()) && $model->resetPassword()){
                Yii::$app->user->logout();
                return [
                    'title' => "Сменить пароль",
                    'content' => '<span class="text-success">Ваш пароль успешно изменен</span>',
                    'footer' => Html::button('Закрыть', ['class' => 'btn btn-white btn-sm', 'data-dismiss' => "modal"]),
                ];
            } else {
                return [
                    'title' => "Сменить пароль",
                    'content' => $this->renderAjax('reset-password-form', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', ['class' => 'btn btn-white pull-left btn-sm', 'data-dismiss' => "modal"]) .
                        Html::button('Изменить', ['class' => 'btn btn-primary btn-sm', 'type' => "submit"])

                ];
            }
        }
    }

    public function actionTest()
    {
//            $result = Yii::$app->mailer->compose()
//    ->setFrom('medichi.notifications@mail.ru')
//    ->setTo('qist27000@gmail.com')
//    ->setSubject('Тема сообщения')
//    ->setTextBody('Текст сообщения')
//    ->setHtmlBody('<b>текст сообщения в формате HTML</b>')
//    ->send();

//        var_dump($result);
    }

    /**
     * @param int $id
     * @param string $hash
     * @return Response
     */
    public function actionConfirm($id, $hash)
    {
        /** @var User $user */
        $user = User::find()->where(['id' => $id, 'password_hash' => $hash])->one();

        if($user){
            $user->email_approved = 1;
            $user->save(false);
            Yii::$app->session->setFlash('success', 'Вы успешно активировали ваш аккаунт');
            return $this->redirect(['site/login']);
        }

        Yii::$app->session->setFlash('error', 'Ссылка не корректна');
        return $this->redirect(['site/login']);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            if (isset($_POST['g-recaptcha-response']) && $_POST['g-recaptcha-response']) {
              $secret = '6Ld13PgZAAAAAII8iRbQeP9wQd7DyHsc2Hikr8Gu';
              $ip = $_SERVER['REMOTE_ADDR'];
              $response = $_POST['g-recaptcha-response'];
              $rsp = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$response&remoteip=$ip");
              $arr = json_decode($rsp, TRUE);
              if ($arr['success']) {

              }
              \Yii::warning($arr, 'ReCaptcha Response');
            }

            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * @return string|Response
     */
    public function actionForgetPassword()
    {
        $this->layout = '@app/views/layouts/main-login';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new ForgetPassword();
        if ($model->load(Yii::$app->request->post()) && $model->sendNewPassword()) {
            Yii::$app->session->setFlash('success', 'На почту отправлен новый пароль');
            return $this->goBack();
        }
        return $this->render('forget-password', [
            'model' => $model,
        ]);
    }

    /**
     * Register action.
     *
     * @return string
     */
    public function actionRegister()
    {
        $this->layout = '@app/views/layouts/main-login.php';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Вы успешно зарегистрировались');
//            (new LoginForm(['username' => $model->email, 'password' => $model->password]))->login();

            if (isset($_POST['g-recaptcha-response']) && $_POST['g-recaptcha-response']) {
              $secret = '6Ld13PgZAAAAAII8iRbQeP9wQd7DyHsc2Hikr8Gu';
              $ip = $_SERVER['REMOTE_ADDR'];
              $response = $_POST['g-recaptcha-response'];
              $rsp = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=$response&remoteip=$ip");
              $arr = json_decode($rsp, TRUE);
              if ($arr['success']) {

              }
              \Yii::warning($arr, 'ReCaptcha Response');
            }

            Yii::$app->session->setFlash('success', 'Письмо активации аккаунта было отправлено на почту');

            return $this->redirect(['site/login']);
        }
        return $this->render('register', [
            'model' => $model,
        ]);
    }

    public function actionSendLoginSms($username)
    {
        $code = rand(1000, 9999);

        \Yii::warning($code, 'Confirm code');

        $user = User::find()->where(['login' => $username])->one();

        if($user){
//            $dataQuery = http_build_query([
//                'api_id' => 'F4768781-AF07-901E-D381-B8158462E570',
//                'to' => $user->phone,
//                'msg' => "Ваш код подтверждения: {$code}",
//                'json' => '1',
//            ]);
//            $smsResponse = json_decode(file_get_contents("https://sms.ru/sms/send?{$dataQuery}"), true);

            Yii::$app->mailer->compose()
                ->setFrom('medichi.notifications@mail.ru')
                ->setTo($user->login)
                ->setSubject('Код подтверждения')
                ->setHtmlBody("Ваш код подтверждения: {$code}")
                ->send();

//            \Yii::warning($smsResponse);

            $user->confirm_code = $code;
            $user->save(false);
        }
    }

    public function actionSendSms($email)
    {
        $code = rand(1000, 9999);
        \Yii::warning($code, 'Confirm code');

//        $dataQuery = http_build_query([
//            'api_id' => 'F4768781-AF07-901E-D381-B8158462E570',
//            'to' => $phone,
//            'msg' => "Ваш код подтверждения: {$code}",
//            'json' => '1',
//        ]);
//        $smsResponse = json_decode(file_get_contents("https://sms.ru/sms/send?{$dataQuery}"), true);

//        \Yii::warning($smsResponse);

        Yii::$app->mailer->compose()
            ->setFrom('medichi.notifications@mail.ru')
            ->setTo($email)
            ->setSubject('Код подтверждения')
            ->setHtmlBody("Ваш код подтверждения: {$code}")
            ->send();

        Yii::$app->session->set('confirm_code', $code);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionCardInfo()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'title' => '',
            'content' => '<p>Пополните на эту карту: XXXX XXXX XXXX XXXX</p>',
            'footer' => Html::button('Закрыть',['class'=>'btn btn-light btn-block','data-dismiss'=>"modal"]),
        ];
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionApplications()
    {
        return $this->render('@app/views/_prototypes/applications');
    }

    public function actionAuto()
    {
        return $this->render('@app/views/_prototypes/auto');
    }

    public function actionAutoView()
    {
        return $this->render('@app/views/_prototypes/auto_view');
    }
}
