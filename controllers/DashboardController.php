<?php

namespace app\controllers;

use app\behaviors\UpdateOnlineBehavior;
use app\models\forms\ConfirmForm;
use app\models\Order;
use app\models\User;
use Yii;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\Response;

/**
 * UserController implements the CRUD actions for User model.
 */
class DashboardController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            ['class' => UpdateOnlineBehavior::className()],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    // allow authenticated users
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    /**
     * Render Yandex maps.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @param int $userId
     * @param float $amount
     * @param string $hash
     * @return mixed
     */
    public function actionTransactionSuccess($userId, $amount, $hash)
    {
        $order = Order::find()->where(['internal_hash' => $hash, 'status' => Order::STATUS_PRE])->one();

        if($order){
            $order->status = Order::STATUS_PLAN;
            $order->type = Order::TYPE_REPLENISHMENT;
            $order->value = $amount;
            $order->user_id = $userId;
            $order->created_at = date('Y-m-d H:i:s');
            $order->save(false);

            $user = User::findOne(Yii::$app->user->identity->getId());
            if($user){
                $user->balance_usd = $user->balance_usd + $amount;
                $user->save(false);
            }

            Yii::$app->session->setFlash('info', 'Транзакция прошла успешно');
        }

        return $this->redirect(['index']);
    }

    /**
     * Render Yandex maps.
     * @return mixed
     */
    public function actionInvesting()
    {
        return $this->render('investing');
    }

    /**
     * Render Yandex maps.
     * @return mixed
     */
    public function actionCompanyStatistic()
    {
        return $this->render('company-statistic');
    }

    /**
     * Render Yandex maps.
     * @return mixed
     */
    public function actionStatistic()
    {
        return $this->render('statistic');
    }

    /**
     * @return string
     */
    public function actionPayment()
    {
        return $this->render('payment');
    }

    /**
     * @param string $type
     */
    public function actionConfirm($type)
    {
        $request = \Yii::$app->request;
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new ConfirmForm();


        if($request->isGet){

            /** @var User $user */
            $user = Yii::$app->user->identity;
            $user->confirm_code = rand(1000, 9999);
            $user->save(false);

            $dataQuery = http_build_query([
                'api_id' => 'F4768781-AF07-901E-D381-B8158462E570',
                'to' => $user->phone,
                'msg' => "Ваш код подтверждения: {$user->confirm_code}",
                'json' => '1',
            ]);
            $smsResponse = json_decode(file_get_contents("https://sms.ru/sms/send?{$dataQuery}"), true);


            \Yii::warning($smsResponse);

            return [
                'title'=> "Подтверждение",
                'content'=>$this->renderAjax('confirm', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Подтвердить',['class'=>'btn btn-primary','type'=>"submit"])

            ];
        }else if($model->load($request->post()) && $model->check()){

            if($type == 1){
                return [
                    'title'=> "Подтверждение",
                    'content'=> '<span class="text-success">Переадресация</span><script>window.location.href = "http://vk.cc/aBWU2R";</script>',
                    'footer'=> ''
                ];
            } else if($type == 2){
                return [
                    'title'=> "Подтверждение",
                    'content'=> '<span class="text-success">Переадресация</span><script>window.location.href = "http://vk.cc/aBWWsT";</script>',
                    'footer'=> ''
                ];
            }
        }else{
            return [
                'title'=> "Подтверждение",
                'content'=>$this->renderAjax('confirm', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Подтвердить',['class'=>'btn btn-primary','type'=>"submit"])

            ];
        }
    }

    /**
     * Render Yandex maps.
     * @return mixed
     */
    public function actionAccount()
    {
        return $this->render('account');
    }
}
