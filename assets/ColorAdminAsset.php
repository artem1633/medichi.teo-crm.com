<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ColorAdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web/theme';
    public $css = [
        // 'http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700',
        'assets/plugins/simplebar/css/simplebar.css',
        'assets/css/bootstrap.min.css',
        'assets/css/animate.css',
        'assets/css/icons.css',
        'assets/css/sidebar-menu.css',
        'assets/css/app-style.css',
        'assets/plugins/bootstrap-wizard/css/bwizard.min.css',
        'assets/css/skins.css',
//        'assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css',
//        'assets/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css',
//        'assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css',
//        'assets/plugins/DataTables/extensions/AutoFill/css/autoFill.bootstrap.min.css',
//        'assets/plugins/DataTables/extensions/ColReorder/css/colReorder.bootstrap.min.css',
//        'assets/plugins/DataTables/extensions/KeyTable/css/keyTable.bootstrap.min.css',
//        'assets/plugins/DataTables/extensions/RowReorder/css/rowReorder.bootstrap.min.css',
//        'assets/plugins/DataTables/extensions/Select/css/select.bootstrap.min.css',
    ];
    public $js = [
//        'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js',
//        'assets/js/jquery.min.js',
        'assets/js/popper.min.js',
        'assets/js/bootstrap.min.js',
        'assets/plugins/simplebar/js/simplebar.js',
        'assets/js/sidebar-menu.js',
        'assets/js/app-script.js',
        'assets/js/table-manage-combine.demo.min.js',
        'assets/js/apps.min.js',
        'assets/plugins/apexcharts/apexcharts.js',
        'assets/js/index.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
         'yii\bootstrap\BootstrapAsset',
    ];
}
